import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
import json
import time
from pprint import pprint
import csv

def web_scrap_blur():
    # Initialize a Chrome WebDriver instance
    service = Service(ChromeDriverManager().install())
    driver = webdriver.Chrome(service=service)

    # URL from your question
    url1 = "https://core-api.prod.blur.io/v1/collections/azuki/tokens?filters=%7B%22traits%22%3A%5B%5D%2C%22hasAsks%22%3Atrue%7D"
    url2 = 'https://core-api.prod.blur.io/v1/collections/azuki/tokens?filters=%7B%22cursor%22%3A%7B%22price%22%3A%7B%22unit%22%3A%22ETH%22%2C%22amount%22%3A%2220%22%2C%22listedAt%22%3A%222023-12-12T03%3A02%3A24.000Z%22%7D%2C%22tokenId%22%3A%226583%22%7D%2C%22traits%22%3A%5B%5D%2C%22hasAsks%22%3Atrue%7D'
    url3 = 'https://core-api.prod.blur.io/v1/collections/azuki/tokens?filters=%7B%22cursor%22%3A%7B%22price%22%3A%7B%22unit%22%3A%22ETH%22%2C%22amount%22%3A%228%22%2C%22listedAt%22%3A%222024-01-02T00%3A37%3A42.206Z%22%7D%2C%22tokenId%22%3A%22860%22%7D%2C%22traits%22%3A%5B%5D%2C%22hasAsks%22%3Atrue%7D'

    url_list = [url1, url2, url3]
    nft_list = []

    for i in url_list:
        # Use Selenium to get the page
        driver.get(i)

        # Wait for the page to load
        time.sleep(2) # Sleeps are generally bad practice; you should use explicit waits

        # After the page is loaded, get the page source which contains the JSON
        # Assuming the JSON is displayed in the page's body
        page_source = driver.find_element(By.TAG_NAME, "body").text

        # Close the WebDriver instance


        # Load the JSON string into a Python dictionary
        data = json.loads(page_source)

        # Print the fetched data
        pprint(data)


        table = data['tokens']

        for nft in table:
            nft = nft['name']
            nft = nft.replace('Azuki #', '')
            nft_list.append(nft)


    driver.quit()
    print(nft_list)
    print(f'items in nft list = {len(nft_list)}')




def check_green_bean_claimed():
    # The URL where you check the green bean status (replace with the actual URL)
    check_url = "https://api.greenbean.devzukis.com/v1/can-claim"

    # Navigate to the URL
    response = requests.get(check_url)
    response = response.json()
    pprint(response)
    for i in response:
        can_claim.append(i['tokenId'])

# check_green_bean_claimed()


can_claim = [0, 4, 6, 7, 10, 11, 13, 15, 16, 20, 21, 22, 24, 25, 26, 27, 29, 30, 31, 33, 34, 38, 39, 40, 41, 42, 43, 46, 48, 50, 55, 57, 59, 60, 63, 65, 66, 67, 70, 71, 72, 74, 76, 78, 79, 80, 82, 83, 84, 85, 86, 87, 89, 90, 92, 101, 103, 104, 107, 111, 112, 116, 118, 119, 120, 121, 122, 126, 128, 130, 131, 132, 134, 135, 136, 138, 139, 143, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 159, 160, 164, 166, 168, 169, 171, 173, 174, 175, 176, 177, 180, 188, 189, 242, 262, 294, 296, 308, 315, 433, 466, 473, 536, 575, 604, 677, 699, 754, 755, 761, 816, 854, 870, 965, 994, 1018, 1064, 1280, 1284, 1329, 1341, 1385, 1417, 1471, 1583, 1614, 1617, 1638, 1696, 1706, 1712, 1768, 1772, 1789, 1796, 1808, 1854, 1868, 1938, 1972, 2039, 2105, 2109, 2127, 2146, 2181, 2202, 2203, 2226, 2246, 2366, 2380, 2474, 2490, 2493, 2510, 2518, 2528, 2535, 2540, 2544, 2558, 2604, 2624, 2679, 2688, 2700, 2724, 2730, 2751, 2756, 2776, 2782, 2798, 2853, 2924, 2985, 3169, 3259, 3270, 3282, 3283, 3305, 3315, 3342, 3350, 3401, 3439, 3469, 3534, 3541, 3615, 3652, 3713, 3733, 3743, 3770, 3816, 3837, 3880, 3917, 3965, 3980, 3991, 4050, 4054, 4056, 4200, 4244, 4265, 4337, 4452, 4503, 4586, 4594, 4674, 4687, 4731, 4754, 4782, 4834, 4951, 4992, 5011, 5018, 5024, 5158, 5177, 5203, 5229, 5240, 5254, 5283, 5299, 5310, 5337, 5372, 5377, 5406, 5432, 5458, 5531, 5545, 5637, 5689, 5697, 5714, 5761, 5807, 5840, 5882, 5885, 5895, 5944, 5978, 5994, 6066, 6138, 6182, 6205, 6245, 6248, 6331, 6332, 6352, 6370, 6526, 6555, 6594, 6600, 6615, 6624, 6629, 6639, 6673, 6674, 6694, 6701, 6703, 6758, 6806, 6813, 6828, 6829, 6915, 6951, 7006, 7049, 7135, 7175, 7224, 7251, 7282, 7300, 7319, 7334, 7335, 7370, 7454, 7466, 7485, 7491, 7499, 7595, 7602, 7644, 7653, 7660, 7666, 7671, 7678, 7758, 7787, 7802, 7822, 7833, 7844, 7902, 7912, 7919, 7971, 8000, 8038, 8092, 8128, 8134, 8137, 8208, 8209, 8302, 8310, 8319, 8386, 8415, 8454, 8474, 8553, 8563, 8595, 8600, 8645, 8689, 8701, 8768, 8830, 8955, 8993, 9096, 9137, 9153, 9159, 9258, 9296, 9372, 9411, 9448, 9454, 9536, 9542, 9565, 9613, 9697, 9750, 9759, 9763, 9784, 9822, 9845, 9848, 9851, 9871, 9923, 9942, 9943, 9954, 9971, 9975]
nft_list = [1914, 6876, 3254, 5385, 399, 7132, 2873, 4344, 2104, 2583, 7907, 611, 9840, 8791, 3467, 1900, 8409, 9177, 7384, 673, 9714, 894, 3594, 637, 4773, 4052, 7068, 2084, 5642, 3833, 1375, 7073, 9463, 2301, 9342, 944, 5001, 2876, 5151, 1490, 6972, 2997, 1257, 7883, 1714, 4313, 8586, 2579, 8254, 569, 4220, 5428, 4713, 2961, 6890, 9357, 4760, 1130, 5327, 2095, 2307, 7454, 1767, 4252, 3479, 6757, 8575, 6971, 9704, 3067, 5290, 8403, 7359, 8512, 2569, 8440, 7607, 9352, 4035, 5507, 8743, 4013, 8133, 247, 1353, 5129, 8566, 6356, 7537, 9875, 7174, 2013, 2871, 541, 8579, 7484, 7786, 8047, 6939, 9317, 723, 9612, 4515, 1027, 137, 2605, 9998, 1682, 2979, 3480, 4884, 5193, 5533, 5932, 7202, 6161, 4684, 4944, 4368, 630, 9904, 3635, 8418, 2438, 6698, 9623, 2577, 5055, 4893, 4913, 8142, 7249, 1781, 4491, 232, 7451, 783, 1760, 3169, 8307, 8583, 858, 9587, 2899, 9166, 9395, 9616, 1102, 8074, 1932, 6653, 7899, 5277, 666, 7119, 1825, 4765, 8433, 6292, 308, 6536, 2193, 5627, 6494, 9600, 9211, 4332, 7626, 1530, 9665, 3656, 7768, 1163, 6702, 2987, 2947, 8293, 2938, 3686, 5253, 9042, 3763, 4213, 2480, 2596, 5027, 2623, 1100, 7715, 8513, 8015, 3013, 4128, 4835, 3088, 6540, 3167, 7445, 6647, 4596, 8434, 1734, 6208, 6307, 9497, 4399, 8238, 7087, 8354, 1623, 8640, 1911, 3086, 946, 4392, 2303, 4938, 5895, 2016, 3001, 5442, 6129, 9217, 7281, 5111, 7230, 5149, 2466, 5108, 369, 5679, 6229, 8007, 1648, 5346, 743, 756, 4342, 3746, 5470, 6875, 794, 8842, 7896, 4244, 4616, 5578, 5596, 7053, 7547, 8528, 8856, 9115, 1261, 2818, 3428, 3607, 4184, 4279, 507, 5978, 6962, 7047, 3846, 6666, 7728, 6475, 8285, 3416]

buy_to_claim = list(set(can_claim) & set(nft_list))

print(buy_to_claim)


