import time
from pprint import pprint
import requests
import pandas as pd
import numpy as np
from CEX_Request_V3 import ByBitRequest, Support_Function
import pal
import datetime


pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', 1000)

bybit_key = pal.bybit_key_3
bybit_secret = pal.bybit_secret_3
bybit_request = ByBitRequest(bybit_key, bybit_secret)
support_function = Support_Function()

#149 symbol
trade_symbol = ['ACEUSDT', 'ACHUSDT', 'AGIXUSDT', 'AGLDUSDT', 'AIUSDT', 'ALICEUSDT', 'ALPACAUSDT', 'ALPHAUSDT', 'ANKRUSDT', 'APEUSDT', 'ARKUSDT', 'ARPAUSDT', 'ARUSDT', 'ATAUSDT', 'AUCTIONUSDT', 'AUDIOUSDT', 'AVAXUSDT', 'AXSUSDT', 'BADGERUSDT', 'BALUSDT', 'BCHUSDT', 'BELUSDT', 'BLURUSDT', 'BNTUSDT', 'BNXUSDT', 'CFXUSDT', 'CHZUSDT', 'CKBUSDT', 'COMBOUSDT', 'COMPUSDT', 'CRVUSDT', 'CTKUSDT', 'CVCUSDT', 'CVXUSDT', 'CYBERUSDT', 'DASHUSDT', 'DATAUSDT', 'DOGEUSDT', 'DUSKUSDT', 'DYDXUSDT', 'EDUUSDT', 'EOSUSDT', 'ETCUSDT', 'ETHUSDT', 'FLMUSDT', 'FLOWUSDT', 'FORTHUSDT', 'FTMUSDT', 'FUNUSDT', 'GALAUSDT', 'GALUSDT', 'GASUSDT', 'GLMUSDT', 'GTCUSDT', 'HBARUSDT', 'HFTUSDT', 'HIFIUSDT', 'HIGHUSDT', 'HOOKUSDT', 'ICXUSDT', 'IDUSDT', 'ILVUSDT', 'IMXUSDT', 'IOSTUSDT', 'IOTAUSDT', 'JASMYUSDT', 'JSTUSDT', 'JTOUSDT', 'JUPUSDT', 'KEYUSDT', 'KLAYUSDT', 'KNCUSDT', 'KSMUSDT', 'LEVERUSDT', 'LINAUSDT', 'LINKUSDT', 'LITUSDT', 'LOOMUSDT', 'LPTUSDT', 'LQTYUSDT', 'LRCUSDT', 'LTCUSDT', 'MANAUSDT', 'MANTAUSDT', 'MBLUSDT', 'MDTUSDT', 'MEMEUSDT', 'MINAUSDT', 'MTLUSDT', 'NEOUSDT', 'NFPUSDT', 'NKNUSDT', 'NMRUSDT', 'OCEANUSDT', 'OGNUSDT', 'OMGUSDT', 'ONGUSDT', 'OXTUSDT', 'PENDLEUSDT', 'PEOPLEUSDT', 'PHBUSDT', 'POLYXUSDT', 'PROMUSDT', 'QIUSDT', 'QTUMUSDT', 'RAREUSDT', 'RDNTUSDT', 'REEFUSDT', 'REQUSDT', 'RIFUSDT', 'RLCUSDT', 'RSRUSDT', 'RUNEUSDT', 'RVNUSDT', 'SEIUSDT', 'SFPUSDT', 'SLPUSDT', 'SNTUSDT', 'SNXUSDT', 'SOLUSDT', 'SPELLUSDT', 'SSVUSDT', 'STEEMUSDT', 'STGUSDT', 'STMXUSDT', 'STRAXUSDT', 'SUPERUSDT', 'SUSHIUSDT', 'SXPUSDT', 'THETAUSDT', 'TRUUSDT', 'TRXUSDT', 'TWTUSDT', 'UMAUSDT', 'UNFIUSDT', 'USTCUSDT', 'VETUSDT', 'VGXUSDT', 'WOOUSDT', 'XAIUSDT', 'XLMUSDT', 'XMRUSDT', 'XRPUSDT', 'XTZUSDT', 'XVGUSDT', 'YFIUSDT', 'YGGUSDT', 'ZECUSDT', 'ZRXUSDT']


threshold = 0.0002
category = 'linear'
symbol = 'BTCUSDT'

def position_count():
    position = bybit_request.get_position_info(category=category, settleCoin='USDT')
    position_list = position['result']['list']
    total_position = len(position_list)

    return total_position


def calculate_sleep_time():
    current_time = datetime.datetime.now()
    # Calculate the next run hour by finding the next multiple of 8 hours.
    next_run_hour = ((current_time.hour + 8) // 8) * 8

    # If the next run hour is 24, set the next run time to midnight of the next day.
    if next_run_hour == 24:
        # Add one day to the current date and set time to midnight.
        next_run_time = (current_time + datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
    else:
        # Create the next run time for today with the next run hour.
        next_run_time = current_time.replace(hour=next_run_hour, minute=0, second=0, microsecond=0)
        # If the next run time is less than or equal to the current time, it means the next run time is tomorrow.
        if next_run_time <= current_time:
            # Add 8 hours to the current next_run_time to ensure the next_run_time is in the future.
            next_run_time += datetime.timedelta(hours=8)

    # Calculate the number of seconds to sleep.
    sleep_seconds = (next_run_time - current_time).total_seconds()

    return sleep_seconds


existing_short_position = []
existing_long_position = []



while True:
    current_time = datetime.datetime.now()
    if current_time.hour == (0, 8, 16) and current_time.minute < 5:
        time.sleep(120)
        # trade logic
        new_short_position = []
        new_long_position = []

        for symbol in trade_symbol:
            data = bybit_request.get_funding_rate_history(category=category, symbol=symbol)
            funding_rate = float(data['list'][0]['fundingRate'])
            if funding_rate > threshold:
                new_short_position.append(symbol)
            if funding_rate < -threshold:
                new_long_position.append(symbol)

        print(f'new_short_position = {new_short_position}')
        print(f'new_long_position = {new_long_position}')

        # check exisitng position
        position = bybit_request.get_position_info(category=category, settleCoin='USDT')
        position = position['result']['list']

        for info in position:
            symbol = info['symbol']
            side = info['side']  # Buy, Sell, None

            if side == 'Sell':
                existing_short_position.append(symbol)
            elif side == 'Buy':
                existing_long_position.append(symbol)


        new_long = set(new_long_position) - set(existing_long_position)
        removed_long = set(existing_long_position) - set(new_long_position)
        new_short = set(new_short_position) - set(existing_short_position)
        removed_short = set(existing_short_position) - set(new_short_position)

        print(f'new_long = {new_long}')
        print(f'removed_long = {removed_long}')
        print(f'new_short = {new_short}')
        print(f'removed_short = {removed_short}')

        for symbol in new_long:
            print(f'new long for {symbol}')
            total_position = position_count()
            if total_position < 10:
                bid, ask = bybit_request.fetch_bybit_data(symbol=symbol, category='linear')
                size = round(20 / bid, 1) # send order base on adjusted size
                print(f'sending {symbol} buy order')
                response = bybit_request.place_market_order(category='linear', side='Buy', symbol=symbol,
                                                            order_type='Market', qty=size)
                message = f'Funding Rate Trade\nBybit Open Long {symbol}\nRef Price = {ask}\nsize = {size}\n\n{response})'
                print(message)
                support_function.tg_pop(message)

        for symbol in removed_long:
            print(f'reoved long for {symbol}')
            print(f'sending {symbol} sell order')
            bid, ask = bybit_request.fetch_bybit_data(symbol=symbol, category='linear')
            position = bybit_request.get_position_info(category=category, symbol=symbol)
            size = position['result']['list'][0]['size'] # read the existing position size
            response = bybit_request.place_market_order(category='linear', side='Sell', symbol=symbol,
                                                        order_type='Market', qty=size)
            message = f'Funding Rate Trade\nBybit Settle Long {symbol}\nRef Price = {bid}\nsize = {size}\n\n{response})'
            print(message)
            support_function.tg_pop(message)

        for symbol in new_short:
            print(f'new short for {symbol}')
            total_position = position_count()
            if total_position < 10:
                bid, ask = bybit_request.fetch_bybit_data(symbol=symbol, category='linear')
                size = round(20 / bid, 1) # send order base on adjusted size
                response = bybit_request.place_market_order(category='linear', side='Sell', symbol=symbol,
                                                            order_type='Market', qty=size)
                message = f'Funding Rate Trade\nBybit Open Short {symbol}\nRef Price = {bid}\nsize = {size}\n\n{response})'
                print(message)
                support_function.tg_pop(message)

        for symbol in removed_short:
            print(f'remove short for {symbol}')
            print(f'sending {symbol} buy order')
            bid, ask = bybit_request.fetch_bybit_data(symbol=symbol, category='linear')
            position = bybit_request.get_position_info(category=category, symbol=symbol)
            size = position['result']['list'][0]['size'] # read the existing position size
            response = bybit_request.place_market_order(category='linear', side='Buy', symbol=symbol,
                                                        order_type='Market', qty=size)
            message = f'Funding Rate Trade\nBybit Settle Short {symbol}\nRef Price = {ask}\nsize = {size}\n\n{response})'
            print(message)
            support_function.tg_pop(message)

        # for testing only
        # existing_long_position = new_long_position
        # existing_short_position = new_short_position

        print('=========== end =============')

        time_to_sleep = calculate_sleep_time()
        time.sleep(time_to_sleep)
    else:
        time_to_sleep = calculate_sleep_time()
        time.sleep(time_to_sleep)


