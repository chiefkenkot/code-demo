import pandas as pd
import numpy as np

class strategy():

    def __int__(self):
        pass

    def bband_momentum(self, x,y, timeframe, csv_name):
        df = pd.read_csv(csv_name)
        df = df[['Date', 'close']]
        df['pct_change'] = df['close'].pct_change()

        df['ma'] = df['close'].rolling(x).mean()
        df['sd'] = df['close'].rolling(x).std()
        df['z'] = (df['close'] - df['ma']) / df['sd']

        df['pos'] = np.where(df['z'] > y, 1, np.where(df['z'] < -y, -1, 0))

        df['pos_t-1'] = df['pos'].shift(1)
        df['trade'] = abs(df['pos_t-1'] - df['pos'])
        df['cost'] = df['trade'] * 0.05 / 100
        df['pnl'] = df['pos_t-1'] * df['pct_change'] - df['cost']
        df['cumu'] = df['pnl'].cumsum()

        ### buy & hold cumu
        df['bnh_pnl'] = df['pct_change']
        df.loc[0:x-1,'bnh_pnl'] = 0
        df['bnh_cumu'] = df['bnh_pnl'].cumsum()

        if timeframe == '15min':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*24*4) ## 15m
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 24 * 4)  ## 15m
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 24 * 4), 2)  ## 15m

        elif timeframe == '1h':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*24)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365*24)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 24), 2)

        elif timeframe == '4h':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*6)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 6)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 6), 2)

        elif timeframe == '1d':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365), 2)

        df['dd'] = df['cumu'].cummax() - df['cumu']
        mdd = round(df['dd'].max(),3)
        calmar = round(annual_return / mdd,2)
        # print(x, y, 'sharpe', sharpe, 'annual_return', annual_return, 'mdd', mdd, 'calmar', calmar)
        # print(f'x = {x}, y = {y}, sharpe = {sharpe}, annual_return = {annual_return}, mdd = {mdd}, calmar = {calmar}')

        stats = pd.Series([x, y, sharpe, annual_return, mdd, calmar], index=['x', 'y', 'sharpe', 'annual_return', 'mdd', 'calmar'])

        return stats, df


    def bband_reversion(self, x, y, timeframe, csv_name):
        df = pd.read_csv(csv_name)
        df = df[['Date', 'close']]
        df['pct_change'] = df['close'].pct_change()

        df['ma'] = df['close'].rolling(x).mean()
        df['sd'] = df['close'].rolling(x).std()
        df['z'] = (df['close'] - df['ma']) / df['sd']

        df['pos'] = np.where(df['z'] > y, -1, np.where(df['z'] < -y, 1, 0))

        df['pos_t-1'] = df['pos'].shift(1)
        df['trade'] = abs(df['pos_t-1'] - df['pos'])
        df['cost'] = df['trade'] * 0.05 / 100
        df['pnl'] = df['pos_t-1'] * df['pct_change'] - df['cost']
        df['cumu'] = df['pnl'].cumsum()

        ### buy & hold cumu
        df['bnh_pnl'] = df['pct_change']
        df.loc[0:x-1,'bnh_pnl'] = 0
        df['bnh_cumu'] = df['bnh_pnl'].cumsum()

        if timeframe == '15min':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*24*4) ## 15m
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 24 * 4)  ## 15m
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 24 * 4), 2)  ## 15m

        elif timeframe == '1h':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*24)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365*24)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 24), 2)

        elif timeframe == '4h':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*6)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 6)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 6), 2)

        elif timeframe == '1d':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365), 2)

        df['dd'] = df['cumu'].cummax() - df['cumu']
        mdd = round(df['dd'].max(),3)
        calmar = round(annual_return / mdd,2)
        # print(f'x = {x}, y = {y}, sharpe = {sharpe}, annual_return = {annual_return}, mdd = {mdd}, calmar = {calmar}')

        stats = pd.Series([x, y, sharpe, annual_return, mdd, calmar], index=['x', 'y', 'sharpe', 'annual_return', 'mdd', 'calmar'])

        return stats, df


    def atr_momentum(self, x,y, timeframe, csv_name):
        df = pd.read_csv(csv_name)
        df = df[['Date', 'high', 'low', 'close', 'open']]
        df['pct_change'] = df['close'].pct_change()

        # Calculate True Range and Average True Range
        df['high_low'] = df['high'] - df['low']
        df['high_close'] = abs(df['high'] - df['close'].shift())
        df['low_close'] = abs(df['low'] - df['close'].shift())
        df['tr'] = df[['high_low', 'high_close', 'low_close']].max(axis=1)
        df['atr'] = df['tr'].rolling(x).mean()

        # Define entry signals
        df['ma'] = df['close'].rolling(x).mean()
        df['upper_band'] = df['ma'] + df['atr'] * y
        df['lower_band'] = df['ma'] - df['atr'] * y
        df['long_entry'] = df['close'] > df['upper_band']
        df['short_entry'] = df['close'] < df['lower_band']

        # Define position logic (1 for long, -1 for short, 0 for neutral)
        df['pos'] = np.where(df['long_entry'], 1, np.where(df['short_entry'], -1, 0))

        df['pos_t-1'] = df['pos'].shift(1)
        df['trade'] = abs(df['pos_t-1'] - df['pos'])
        df['cost'] = df['trade'] * 0.05 / 100
        df['pnl'] = df['pos_t-1'] * df['pct_change'] - df['cost']
        df['cumu'] = df['pnl'].cumsum()

        ### buy & hold cumu
        df['bnh_pnl'] = df['pct_change']
        df.loc[0:x-1,'bnh_pnl'] = 0
        df['bnh_cumu'] = df['bnh_pnl'].cumsum()

        if timeframe == '15min':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*24*4) ## 15m
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 24 * 4)  ## 15m
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 24 * 4), 2)  ## 15m

        elif timeframe == '1h':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*24)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365*24)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 24), 2)

        elif timeframe == '4h':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*6)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 6)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 6), 2)

        elif timeframe == '1d':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365), 2)

        df['dd'] = df['cumu'].cummax() - df['cumu']
        mdd = round(df['dd'].max(),3)
        calmar = round(annual_return / mdd,2)
        # print(x, y, 'sharpe', sharpe, 'annual_return', annual_return, 'mdd', mdd, 'calmar', calmar)
        # print(f'x = {x}, y = {y}, sharpe = {sharpe}, annual_return = {annual_return}, mdd = {mdd}, calmar = {calmar}')

        stats = pd.Series([x, y, sharpe, annual_return, mdd, calmar], index=['x', 'y', 'sharpe', 'annual_return', 'mdd', 'calmar'])

        return stats, df

    def cross_asset_bband_momentum(self, x, y, timeframe, csv_name_1, csv_name_2):
        df1 = pd.read_csv(csv_name_1)
        df1 = df1[['Date', 'close']]

        df2 = pd.read_csv(csv_name_2)
        df2 = df2[['Date', 'close']]
        df2 = df2.rename(columns={'close':'value'})

        merge_df = pd.merge(df1, df2, on='Date', how='inner')

        merge_df['pct_change'] = merge_df['value'].pct_change()

        df = merge_df

        df['ma'] = df['close'].rolling(x).mean()
        df['sd'] = df['close'].rolling(x).std()
        df['z'] = (df['close'] - df['ma']) / df['sd']

        df['pos'] = np.where(df['z'] > y, 1, np.where(df['z'] < -y, -1, 0))

        df['pos_t-1'] = df['pos'].shift(1)
        df['trade'] = abs(df['pos_t-1'] - df['pos'])
        df['cost'] = df['trade'] * 0.05 / 100
        df['pnl'] = df['pos_t-1'] * df['pct_change'] - df['cost']
        df['cumu'] = df['pnl'].cumsum()

        ### buy & hold cumu
        df['bnh_pnl'] = df['pct_change']
        df.loc[0:x-1,'bnh_pnl'] = 0
        df['bnh_cumu'] = df['bnh_pnl'].cumsum()

        if timeframe == '15min':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*24*4) ## 15m
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 24 * 4)  ## 15m
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 24 * 4), 2)  ## 15m

        elif timeframe == '1h':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*24)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365*24)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 24), 2)

        elif timeframe == '4h':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*6)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 6)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 6), 2)

        elif timeframe == '1d':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365), 2)

        df['dd'] = df['cumu'].cummax() - df['cumu']
        mdd = round(df['dd'].max(),3)
        calmar = round(annual_return / mdd,2)
        # print(x, y, 'sharpe', sharpe, 'annual_return', annual_return, 'mdd', mdd, 'calmar', calmar)
        # print(f'x = {x}, y = {y}, sharpe = {sharpe}, annual_return = {annual_return}, mdd = {mdd}, calmar = {calmar}')

        stats = pd.Series([x, y, sharpe, annual_return, mdd, calmar], index=['x', 'y', 'sharpe', 'annual_return', 'mdd', 'calmar'])

        return stats, df


    def cross_asset_bband_reversion(self, x, y, timeframe, csv_name_1, csv_name_2):
        df1 = pd.read_csv(csv_name_1)
        df1 = df1[['Date', 'close']]

        df2 = pd.read_csv(csv_name_2)
        df2 = df2[['Date', 'close']]
        df2 = df2.rename(columns={'close':'value'})

        merge_df = pd.merge(df1, df2, on='Date', how='inner')

        merge_df['pct_change'] = merge_df['value'].pct_change()

        df = merge_df

        df['ma'] = df['close'].rolling(x).mean()
        df['sd'] = df['close'].rolling(x).std()
        df['z'] = (df['close'] - df['ma']) / df['sd']

        df['pos'] = np.where(df['z'] > y, -1, np.where(df['z'] < -y, 1, 0))

        df['pos_t-1'] = df['pos'].shift(1)
        df['trade'] = abs(df['pos_t-1'] - df['pos'])
        df['cost'] = df['trade'] * 0.05 / 100
        df['pnl'] = df['pos_t-1'] * df['pct_change'] - df['cost']
        df['cumu'] = df['pnl'].cumsum()

        ### buy & hold cumu
        df['bnh_pnl'] = df['pct_change']
        df.loc[0:x-1,'bnh_pnl'] = 0
        df['bnh_cumu'] = df['bnh_pnl'].cumsum()

        if timeframe == '15min':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*24*4) ## 15m
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 24 * 4)  ## 15m
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 24 * 4), 2)  ## 15m

        elif timeframe == '1h':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*24)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365*24)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 24), 2)

        elif timeframe == '4h':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*6)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 6)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 6), 2)

        elif timeframe == '1d':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365), 2)

        df['dd'] = df['cumu'].cummax() - df['cumu']
        mdd = round(df['dd'].max(),3)
        calmar = round(annual_return / mdd,2)
        # print(x, y, 'sharpe', sharpe, 'annual_return', annual_return, 'mdd', mdd, 'calmar', calmar)
        # print(f'x = {x}, y = {y}, sharpe = {sharpe}, annual_return = {annual_return}, mdd = {mdd}, calmar = {calmar}')

        stats = pd.Series([x, y, sharpe, annual_return, mdd, calmar], index=['x', 'y', 'sharpe', 'annual_return', 'mdd', 'calmar'])

        return stats, df

    def cross_asset_bband_merge_pos(self, x1, x2, y1, y2, timeframe, csv_name_1, csv_name_2):
        df1 = pd.read_csv(csv_name_1)
        df1 = df1[['Date', 'close']]

        df2 = pd.read_csv(csv_name_2)
        df2 = df2[['Date', 'close']]
        df2 = df2.rename(columns={'close':'value'})

        merge_df = pd.merge(df1, df2, on='Date', how='inner')

        merge_df['pct_change'] = merge_df['value'].pct_change()

        df = merge_df

        df['ma1'] = df['close'].rolling(x1).mean()
        df['sd1'] = df['close'].rolling(x1).std()
        df['z1'] = (df['close'] - df['ma1']) / df['sd1']

        df['pos1'] = np.where(df['z1'] > y1, 1, np.where(df['z1'] < -y1, -1, 0))

        df['ma2'] = df['close'].rolling(x2).mean()
        df['sd2'] = df['close'].rolling(x2).std()
        df['z2'] = (df['close'] - df['ma2']) / df['sd2']

        df['pos2'] = np.where(df['z2'] > y2, -2, np.where(df['z2'] < -y2, 2, 0))

        df['pos'] = df['pos1'] + df['pos2']

        df['pos_t-1'] = df['pos'].shift(1)
        df['trade'] = abs(df['pos_t-1'] - df['pos'])
        df['cost'] = df['trade'] * 0.05 / 100
        df['pnl'] = df['pos_t-1'] * df['pct_change'] - df['cost']
        df['cumu'] = df['pnl'].cumsum()

        ### buy & hold cumu
        df['bnh_pnl'] = df['pct_change']
        df.loc[0:x1-1,'bnh_pnl'] = 0
        df['bnh_cumu'] = df['bnh_pnl'].cumsum()

        if timeframe == '15min':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*24*4) ## 15m
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 24 * 4)  ## 15m
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 24 * 4), 2)  ## 15m

        elif timeframe == '1h':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*24)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365*24)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 24), 2)

        elif timeframe == '4h':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365*6)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 6)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365 * 6), 2)

        elif timeframe == '1d':
            sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365)
            sharpe = round(sharpe, 2)
            bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365)
            bnh_sharpe = round(bnh_sharpe, 2)
            annual_return = round(df['pnl'].mean() * (365), 2)

        df['dd'] = df['cumu'].cummax() - df['cumu']
        mdd = round(df['dd'].max(),3)
        calmar = round(annual_return / mdd,2)
        # print(x, y, 'sharpe', sharpe, 'annual_return', annual_return, 'mdd', mdd, 'calmar', calmar)
        # print(f'x = {x}, y = {y}, sharpe = {sharpe}, annual_return = {annual_return}, mdd = {mdd}, calmar = {calmar}')

        stats = pd.Series([x1, y1, sharpe, annual_return, mdd, calmar], index=['x', 'y', 'sharpe', 'annual_return', 'mdd', 'calmar'])

        return stats, df