import os.path
import time
from binance.client import Client
import pandas as pd
import requests
from pprint import pprint
from CEX_Request_V3 import ByBitRequest, Support_Function
import pal
import plotly.graph_objs as go
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import datetime

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', 1000)

bybit_key = pal.bybit_key
bybit_secret = pal.bybit_secret
bybit_request = ByBitRequest(bybit_key, bybit_secret)

client = Client(pal.binance_key, pal.binance_secret)


category = 'linear'
symbol = 'AVAXUSDT'
timeframe = '1h'
limit = 200

csv_name = f'{symbol}_{timeframe}.csv'
current_date = datetime.datetime.now()
formatted_date = current_date.strftime("%d %b, %Y")
# Full Cycle
date_from = "12 May, 2020"
# date_from = "5 May, 2023"
date_to = formatted_date

# Worst Time
# date_from = "5 May, 2023"
# date_to = "12 May, 2023"

param_list = [
    {'ROSEUSDT': (800, 2000, 0.25)}, # AR 1.57, SHARPE 1.82
    {'LINAUSDT': (100, 3000, 1.5)}, # AR 1.57, SHARPE 2.47
    {'ILVUSDT': (1500, 3400, 0)}, # AR 1.73, SHARPE 2.17
    {'AVAXUSDT': (400, 3400, 0)}, # AR 1.6, SHARPE 1.89
    {'BAKEUSDT': (100, 3800, 0.5)}, # AR 1.73, SHARPE 1.99
    {'SOLUSDT': (100, 600, 0.25)}, # AR 1.61, SHARPE 1.93
    {'GALAUSDT': (300, 2100, 0.75)}, # AR 1.57, SHARPE 2.16
    {'RUNEUSDT': (100, 2300, 0.25)}, # AR 1.47, SHARPE 2.06
    {'OPUSDT': (500, 400, 0)}, # AR 1.43, SHARPE 2.63
    {'ONEUSDT': (600, 700, 0.5)}, # AR 1.41, SHARPE 2.07
    {'ARBUSDT': (1400, 3300, 0)}, # AR 1.44, SHARPE 2.28
    {'SUIUSDT': (1500, 200, 0.25)}, # AR 1.35, SHARPE 2.1
    {'1INCHUSDT': (2400, 3100, 1)}, # AR 1.35, SHARPE 2.1
    {'NEARUSDT': (800, 2100, 0)}, # AR 1.64, SHARPE 1.9
    {'RNDRUSDT': (2800, 1300, 0)}, # AR 1.73, SHARPE 1.46
    {'FETUSDT': (4600, 1700, 0.25)}, # AR 1.06, SHARPE 1.87
    {'ADAUSDT': (400, 800, 1)}, # AR 1.02, SHARPE 1.98
    {'SANDUSDT': (5900, 1200, 0.5)}, # AR 0.83, SHARPE 1.98
    {'FILUSDT': (1300, 100, 1)}, # AR 0.94, SHARPE 1.57
    {'DOTUSDT': (1000, 3900, 0.75)}, # AR 0.98, SHARPE 1.49
    {'APTUSDT': (200, 1200, 0.5)}, # AR 1.45, SHARPE 2.01 / RARE TRADING CHANCE
    {'IMXUSDT': (3900, 1400, 0)}, # AR 1.16, SHARPE 1.64
    {'MATICUSDT': (200, 600, 0.5)}, # AR 1.05, SHARPE 1.51
    {'FLOWUSDT': (700, 900, 1.5)}, # AR 1.05, SHARPE 1.51
    {'ALGOUSDT': (700, 2500, 0.25)}, # AR 1.01, SHARPE 1.5
    {'MANAUSDT': (800, 1100, 0)}, # AR 0.98, SHARPE 1.45
    {'WOOUSDT': (100, 400, 1)}, # AR 0.98, SHARPE 1.45
    {'ICPUSDT': (3600, 2300, 2)}, # AR 0.83, SHARPE 1.91
    {'COMPUSDT': (100, 1300, 1.5)}, # AR 0.78, SHARPE 1.66
    {'CHZUSDT': (1500, 3300, 0.5)}, # AR 0.8, SHARPE 1.22
    {'ETCUSDT': (900, 2100, 3)},  # AR 0.51, SHARPE 1.82 / RARE TRADING CHANCE

    # {'BTCUSDT': (1300, 1600, 0)}, # AR 0.62, SHARPE 1.2
    # {'EOSUSDT': (200, 2100, 2)}, # AR 0.57, SHARPE 1.02

    # {'SEIUSDT': (2400, 2600, 0.25)}, # AR 3.83, SHARPE 3.88 / NEW LISTED
    # {'BLURUSDT': (2400, 800, 0.5)}, # AR 2.2, SHARPE 3.42 / NEW LISTED, NEED MORE DATA

]

data_list = []

def fetch_data(symbol, timeframe):
    if timeframe == '15min':
        klines_15min = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_15MINUTE, date_from, date_to)
        return klines_15min

    if timeframe == '1h':
        klines_1h = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1HOUR, date_from, date_to)
        return klines_1h

    if timeframe == '4h':
        klines_4h = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_4HOUR, date_from, date_to)
        return klines_4h

    if timeframe == '1d':
        klines_1d = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1DAY, date_from, date_to)
        return klines_1d

def form_data_frame(symbol, csv_name, timeframe):
    df = fetch_data(symbol, timeframe=timeframe) # 15min / 1h / 4h / 1d
    col = ['Date', 'open', 'high', 'low', 'close', 'volume', 'close time', 'Quote asset volume', 'Num trades', 'buy vol', 'buy asset volume', 'ignore.']
    df = pd.DataFrame(df, columns=col)
    df['Date'] = pd.to_datetime(df['Date'], unit='ms')
    df['close'] = pd.to_numeric(df['close'], errors='coerce')
    df.to_csv(csv_name, mode='w', index=False, header=True)

def write_csv(symbol, csv_name, timeframe):
    if not os.path.isfile(csv_name):
        print(f'create new csv for {symbol}')
        form_data_frame(symbol, csv_name, timeframe)
        time.sleep(3)
    else:
        print(f'use existing csv for {symbol}')
        time.sleep(3)

def create_merged_csv():
    def get_oi_data(pages):
        cursor = ''
        for _ in range(pages):
            response = bybit_request.get_open_interest(category=category, symbol=symbol, interval_time=timeframe, limit=limit, cursor=cursor)
            for i in response['list']:
                data_list.append(i)

            if 'nextPageCursor' in response:
                cursor = response['nextPageCursor']
            else:
                break

        print('Total rows:', len(data_list))

        col = ['timestamp', 'openInterest']
        df = pd.DataFrame(data_list, columns=col)
        df = df.rename(columns={'timestamp':'Date'})
        df['Date'] = pd.to_datetime(df['Date'], unit='ms').dt.round('H')
        # df['timestamp'] = pd.to_datetime(df['timestamp'], unit='ms')


        # print(df)
        return df

    if not os.path.isfile(csv_name):
        write_csv(symbol, csv_name, timeframe)
        df2 = pd.read_csv(csv_name)
        df2 = df2[['Date', 'close']]
        df2['Date'] = pd.to_datetime(df2['Date'])
        df2['Date'] = pd.to_datetime(df2['Date'], format='%d/%m/%Y %H:%M')
    else:
        df2 = pd.read_csv(csv_name)
        df2 = df2[['Date', 'close']]
        df2['Date'] = pd.to_datetime(df2['Date'])
        df2['Date'] = pd.to_datetime(df2['Date'], format='%d/%m/%Y %H:%M')

    num_rows = len(df2)
    pages = int(round((num_rows / 200), 0))

    df = get_oi_data(pages)


    merge_df = pd.merge(df, df2, on='Date')
    merge_df = merge_df.sort_values(ascending=True, by='Date').reset_index(drop=True)
    merge_df = merge_df.drop_duplicates(subset='Date', keep='first')
    merge_df.to_csv(f'merge_{symbol}_{timeframe}.csv', index=False)

    return merge_df



#====== plot price vs oi ======
# fig = go.Figure()
#
# fig.add_trace(go.Scatter(x=df['Date'], y=df['close'], name='close'))
# fig.add_trace(go.Scatter(x=df['Date'], y=df['openInterest'], name='oi', yaxis='y2'))
#
# fig.update_layout(
#     title=f'{symbol}_{interval} price vs oi',
#     yaxis=dict(title='close'),
#     yaxis2=dict(title='oi', overlaying='y', side='right')
# )
#
# fig.show()


def price_ma(n):
    df = pd.read_csv(f'merge_{symbol}_{timeframe}.csv')
    df['pct_change'] = df['close'].pct_change()
    df['ma_price'] = df['close'].rolling(n).mean()


    def oi_bband(x, y):
        # global df

        df['ma'] = df['openInterest'].rolling(x).mean()
        df['sd'] = df['openInterest'].rolling(x).std()
        df['z'] = (df['openInterest'] - df['ma']) / df['sd']
        df['+b'] = df['ma'] + (df['sd'] * y)
        df['-b'] = df['ma'] - (df['sd'] * y)

        # df['pos'] = np.where((df['z'] > y), np.where(df['close'] > df['ma_price'], -1, 1),
        #                      np.where(df['z'] < -y, 0, ?))

        # drop & +oi > up
        # up & +oi > drop

        # Open long if z > y and close > ma_price
        long_cond = (df['z'] > y) & ((df['close'] > df['ma_price']))

        # Open short if z > y and close < ma_price
        short_cond = (df['z'] > y) & ((df['close'] < df['ma_price']))

        # Hold if within +/- y band
        hold_cond = (np.abs(df['z']) <= y)

        df['pos'] = 0

        df['pos'] = np.where(long_cond, 1,
                             np.where(short_cond, -1,
                                      np.where(hold_cond, 0, df['pos'])))


        # df['pos'] = np.where((df['z'] > y) & (df['close'] > df['ma_price']), -1,
        #                      np.where((df['z'] > y) & (df['close'] < df['ma_price']), 1, 0))



        df['pos_t-1'] = df['pos'].shift(1)
        df['trade'] = abs(df['pos_t-1'] - df['pos'])
        df['cost'] = df['trade'] * 0.05 / 100
        df['pnl'] = df['pos_t-1'] * df['pct_change'] - df['cost']
        df['cumu'] = df['pnl'].cumsum()

        ### buy & hold cumu
        df['bnh_pnl'] = df['pct_change']
        df.loc[0:x - 1, 'bnh_pnl'] = 0
        df['bnh_cumu'] = df['bnh_pnl'].cumsum()
        df.loc[0:x - 1, 'bnh_cumu'] = 0
        df['bnh_cumu_ma'] = df['bnh_cumu'].rolling(n).mean()

        sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365 * 24)
        sharpe = round(sharpe, 2)
        bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 24)
        bnh_sharpe = round(bnh_sharpe, 2)
        annual_return = round(df['pnl'].mean() * (365 * 24), 2)

        df['dd'] = df['cumu'].cummax() - df['cumu']
        mdd = round(df['dd'].max(), 3)
        calmar = round(annual_return / mdd, 2)
        # print(x, y, 'sharpe', sharpe, 'annual_return', annual_return, 'mdd', mdd, 'calmar', calmar)
        # print(f'x = {x}, y = {y}, sharpe = {sharpe}, annual_return = {annual_return}, mdd = {mdd}, calmar = {calmar}')

        stats = pd.Series([x, y, sharpe, annual_return, mdd, calmar],
                          index=['x', 'y', 'sharpe', 'annual_return', 'mdd', 'calmar'])

        return stats, df

    ma_list = np.arange(100, 4000, 100)
    thres_list = np.arange(0, 2.25, 0.25)

    result_df = pd.DataFrame(columns=['x', 'y', 'sharpe', 'annual_return', 'mdd', 'calmar'])

    for x in ma_list:
        for y in thres_list:
            stats, _ = oi_bband(x, y)
            result_df = result_df._append(stats, ignore_index=True)

    result_df = result_df.sort_values(by='sharpe', ascending=False)
    result_df['n'] = n
    sharpe = result_df['sharpe'].iloc[0]
    annual_return = result_df['annual_return'].iloc[0]
    if sharpe > 1.2:

        x = int(result_df['x'].iloc[0])
        y = float(result_df['y'].iloc[0])
        # x= 2900
        # y= 0

        stats, df = oi_bband(x, y)
        cumu = df['cumu'].iloc[-1]
        bnh_cumu = df['bnh_cumu'].iloc[-1]
        if cumu > bnh_cumu:
            print(result_df.head(10))

            data_table = result_df.pivot(index='x', columns='y', values='sharpe')
            sns.heatmap(data_table, annot=True, fmt='g', cmap='Greens')
            plt.show()

            fig = go.Figure()

            fig.add_trace(go.Scatter(x=df['Date'], y=df['cumu'], mode='lines', name='cumu'))
            fig.add_trace(go.Scatter(x=df['Date'], y=df['dd'], mode='lines', name='dd'))
            fig.add_trace(go.Scatter(x=df['Date'], y=df['bnh_cumu'], mode='lines', name='bnh_cumu'))
            fig.add_trace(go.Scatter(x=df['Date'], y=df['bnh_cumu_ma'], mode='lines', name='bnh_cumu_ma'))
            fig.add_trace(go.Scatter(x=df['Date'], y=df['openInterest'], mode='lines', name='openInterest', yaxis='y2'))
            fig.add_trace(go.Scatter(x=df['Date'], y=df['ma'], mode='lines', name='ma', yaxis='y2'))
            fig.add_trace(go.Scatter(x=df['Date'], y=df['+b'], mode='lines', name='+b', yaxis='y2'))
            fig.add_trace(go.Scatter(x=df['Date'], y=df['-b'], mode='lines', name='-b', yaxis='y2'))

            fig.update_layout(
                title=f'{symbol} oi Strategy price_ma = {n}, x={x}, y={y}, annual_return={annual_return}, sharpe={sharpe}',
                yaxis2=dict(title='openinterest',
                            overlaying='y',
                            side='right',
                            showgrid=False)
            )


            fig.show()

if not os.path.isfile(f'merge_{symbol}_{timeframe}.csv'):
    create_merged_csv()
else:
    print('use existing csv')
    time.sleep(2)

price_ma_list = np.arange(100, 4100, 100)
for n in price_ma_list:
    price_ma(n)