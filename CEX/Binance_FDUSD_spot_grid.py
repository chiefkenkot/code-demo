import datetime
import time
from collections import OrderedDict
from CEX_Request_V3 import BinanceRequest, Support_Function
import pal
from pprint import pprint
from decimal import Decimal, getcontext
import requests


binance_key = pal.binance_key2
binance_secret = pal.binance_secret2
binance_request = BinanceRequest(binance_key, binance_secret)
support_function = Support_Function()

symbol = 'FDUSDUSDT'
upper_price = Decimal('0.9995')  # 0.9990-1.0010
lower_price = Decimal('0.9985')
quantity = 20
grid_quantity = 10
price_increment = Decimal('0.0001')


def fetch_bid_ask_price(symbol):
    ask, bid = binance_request.fetch_binance_data(symbol)
    bid = Decimal(str(bid))
    ask = Decimal(str(ask))

    return bid, ask


def place_order(side, price):
    global quantity
    response = binance_request.place_order(
        symbol='FDUSDUSDT',
        side=side,
        order_type='LIMIT',
        price=price,
        quantity=quantity)

    return response


# Place grid orders
def place_grid_orders(bid, ask):
    global orderid_dict  # Ensure you are modifying the global dictionary
    for i in range(grid_quantity):
        buy_price = lower_price + i * price_increment
        sell_price = upper_price - i * price_increment
        print(f'bid = {bid}, ask = {ask}')
        print(f'buy price = {buy_price}, sell price ={sell_price}')
        print(f'{ask} <= {sell_price} is {ask <= sell_price}')
        print(f'ask ({type(ask)}) = {ask}, sell_price ({type(sell_price)}) = {sell_price}')


        # Place buy orders if the price is above the lower bound
        if bid >= buy_price:
            response = place_order(side='BUY', price=round(buy_price, 4))
            if 'orderId' in response:
                orderid = response['orderId']
                orderid_dict[buy_price] = orderid  # Add to dictionary without string conversion
                print(f'Placed Buy Order @ {buy_price}: {response}')
            else:
                print(f'Buy order placement failed @ {buy_price}: {response}')
            print('=========================================')

        # Place sell orders if the price is below the upper bound
        if ask <= sell_price:
            response = place_order(side='SELL', price=round(sell_price, 4))
            if 'orderId' in response:
                orderid = response['orderId']
                orderid_dict[sell_price] = orderid  # Add to dictionary without string conversion
                print(f'Placed Sell Order @ {sell_price}: {response}')
            else:
                print(f'Sell order placement failed @ {sell_price}: {response}')
            print('=========================================')

    # Create a new OrderedDict sorted by price
    orderid_dict = OrderedDict(sorted(orderid_dict.items()))


def get_wallet_balance():
    free_balance, locked_balance = binance_request.get_account_info()

    # Get balances using .get() with default 0 if key not found
    free_usdt = float(free_balance.get('USDT', '0'))
    free_fdusd = float(free_balance.get('FDUSD', '0'))
    locked_usdt = float(locked_balance.get('USDT', '0'))
    locked_fdusd = float(locked_balance.get('FDUSD', '0'))

    # Sum of free and locked balances
    total_usdt = free_usdt + locked_usdt
    total_fdusd = free_fdusd + locked_fdusd

    return total_usdt, total_fdusd


# ================================================

# response = place_order('BUY', 0.9991)
# pprint(response)
# time.sleep(999999)
# {'code': -2010, 'msg': 'Account has insufficient balance for requested action.'}

# response = place_order('BUY', 0.9995)
# print(response)
# time.sleep(9999)
# Buy order placement failed: {'code': -1111, 'msg': "Parameter 'price' has too much precision."}

# response = place_order('BUY', '0.9990')
# print(response)
# time.sleep(99999)

orderid_dict = {}
cancel_all = binance_request.cancel_all_open_order_on_a_symbol(symbol=symbol)
pprint(cancel_all)
binance_usdt, binance_fdusd = get_wallet_balance()
start_binance_usdt = round(float(binance_usdt), 2)
start_binance_fdusd = round(float(binance_fdusd), 2)
print(
    f'Total Asset: {start_binance_usdt + start_binance_fdusd} ({start_binance_usdt} USDT, {start_binance_fdusd} FDUSD)')
# time.sleep(99999)

bid, ask = fetch_bid_ask_price(symbol)
place_grid_orders(bid, ask)

# Buy order placement failed @ 0.9994999999999999: {'code': -1111, 'msg': "Parameter 'price' has too much precision."}


# fetch all order status

while True:
    try:
        print(f'Time: {datetime.datetime.now()}')
        binance_usdt, binance_fdusd = get_wallet_balance()
        binance_usdt = round(binance_usdt, 2)
        binance_fdusd = round(binance_fdusd, 2)
        print(f'Total Asset: {binance_usdt + binance_fdusd} ({binance_usdt}USDT, {binance_fdusd}FDUSD)')
        print(f'Previous dictionary: {orderid_dict}')
        print(f'Length = {len(orderid_dict)}')
        for order_price, orderid in list(orderid_dict.items()):  # Use list(...) to make a copy of items
            try:
                query_order = binance_request.query_order(symbol=symbol, orderid=orderid)
                status = query_order['status']
                order_side = query_order['side']
                if status == 'FILLED':  # if order executed, place reverse order at +-1
                    if order_side == 'BUY':
                        new_price = Decimal(str(order_price)) + price_increment
                        new_price_str = f"{new_price:.4f}"
                        response = place_order(side='SELL', price=new_price_str)
                        if 'orderId' in response:
                            new_orderid = response['orderId']
                            new_side = response['side']
                            new_orderprice = float(response['price'])
                            orderid_dict[new_orderprice] = new_orderid
                            orderid_dict = OrderedDict(sorted(orderid_dict.items(), key=lambda x: str(x[0])))
                            message = (
                                f'Previous Order {status}: {order_side} @{order_price}\n\nPlaced Reverse {new_side} Order @ {new_orderprice}\n\n'
                                f'Start Total Asset: {start_binance_usdt + start_binance_fdusd}\n ({start_binance_usdt} USDT, {start_binance_fdusd} FDUSD)\n\n'
                                f'Updated Total Asset: {binance_usdt + binance_fdusd}\n ({binance_usdt} USDT, {binance_fdusd} FDUSD) ')
                            support_function.tg_pop(message=message)
                        else:
                            message = (f"Failed to place reverse order for {order_side} @{order_price}\n\nResponse:{response}")
                            support_function.tg_pop(message=message)
                            print(message)
                        del orderid_dict[order_price]
                    elif order_side == 'SELL':
                        new_price = Decimal(str(order_price)) - price_increment
                        new_price_str = f"{new_price:.4f}"
                        response = place_order(side='BUY', price=new_price_str)
                        if 'orderId' in response:
                            new_orderid = response['orderId']
                            new_side = response['side']
                            new_orderprice = float(response['price'])
                            orderid_dict[new_orderprice] = new_orderid
                            orderid_dict = OrderedDict(sorted(orderid_dict.items(), key=lambda x: str(x[0])))
                            message = (
                                f'Previous Order {status}: {order_side} @{order_price}\n\nPlaced Reverse {new_side} Order @ {new_orderprice}\n\n'
                                f'Start Total Asset: {start_binance_usdt + start_binance_fdusd}\n ({start_binance_usdt} USDT, {start_binance_fdusd} FDUSD)\n\n'
                                f'Updated Total Asset: {binance_usdt + binance_fdusd}\n ({binance_usdt} USDT, {binance_fdusd} FDUSD) ')
                            support_function.tg_pop(message=message)
                        else:
                            message = (f"Failed to place reverse order for {order_side} @{order_price}\n\nResponse:{response}")
                            support_function.tg_pop(message=message)
                            print(message)
                        del orderid_dict[order_price]

            except KeyError as e:
                support_function.tg_pop(f'Key Error Occurs: {e}')
                print(f'Key error occurs @ orderid {orderid}')
                cancel_all = binance_request.cancel_all_open_order_on_a_symbol(symbol=symbol)
                pprint(cancel_all)
                orderid_dict = {}
                bid, ask = fetch_bid_ask_price(symbol)
                place_grid_orders(bid, ask)
                continue

            except ConnectionResetError:
                support_function.tg_pop('ConnectionResetError Occurs')
                cancel_all = binance_request.cancel_all_open_order_on_a_symbol(symbol=symbol)
                pprint(cancel_all)
                orderid_dict = {}
                bid, ask = fetch_bid_ask_price(symbol)
                place_grid_orders(bid, ask)
                continue
            except ConnectionError:
                support_function.tg_pop('ConnectionError Occurs')
                cancel_all = binance_request.cancel_all_open_order_on_a_symbol(symbol=symbol)
                pprint(cancel_all)
                orderid_dict = {}
                bid, ask = fetch_bid_ask_price(symbol)
                place_grid_orders(bid, ask)
                continue

            except requests.exceptions.RequestException as e:
                support_function.tg_pop('ConnectionResetError Occurs')
                cancel_all = binance_request.cancel_all_open_order_on_a_symbol(symbol=symbol)
                pprint(cancel_all)
                orderid_dict = {}
                bid, ask = fetch_bid_ask_price(symbol)
                place_grid_orders(bid, ask)
                continue

        print(f'Updated dictionary: {orderid_dict}')
        print('========================================= Next Loop =========================================')

    except ConnectionResetError:
        support_function.tg_pop('ConnectionResetError Occurs')
        cancel_all = binance_request.cancel_all_open_order_on_a_symbol(symbol=symbol)
        pprint(cancel_all)
        orderid_dict = {}
        bid, ask = fetch_bid_ask_price(symbol)
        place_grid_orders(bid, ask)
        continue
    except ConnectionError:
        support_function.tg_pop('ConnectionError Occurs')
        cancel_all = binance_request.cancel_all_open_order_on_a_symbol(symbol=symbol)
        pprint(cancel_all)
        orderid_dict = {}
        bid, ask = fetch_bid_ask_price(symbol)
        place_grid_orders(bid, ask)
        continue

    except KeyError as e:
        support_function.tg_pop(f'Key Error Occurs: {e}')
        cancel_all = binance_request.cancel_all_open_order_on_a_symbol(symbol=symbol)
        pprint(cancel_all)
        orderid_dict = {}
        bid, ask = fetch_bid_ask_price(symbol)
        place_grid_orders(bid, ask)
        continue

    except requests.exceptions.RequestException as e:
        support_function.tg_pop('ConnectionResetError Occurs')
        cancel_all = binance_request.cancel_all_open_order_on_a_symbol(symbol=symbol)
        pprint(cancel_all)
        orderid_dict = {}
        bid, ask = fetch_bid_ask_price(symbol)
        place_grid_orders(bid, ask)
        continue
