import os.path
import time
from binance.client import Client
import pandas as pd
import requests
from pprint import pprint
from CEX_Request_V3 import ByBitRequest, Support_Function
import pal
import numpy as np

import datetime

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', 1000)

bybit_key = pal.bybit_key_2
bybit_secret = pal.bybit_secret_2
bybit_request = ByBitRequest(bybit_key, bybit_secret)

client = Client(pal.binance_key, pal.binance_secret)
support_function = Support_Function()

category = 'linear'
# symbol = 'RNDRUSDT'
timeframe = '1h'
limit = 200

# csv_name = f'{symbol}_{timeframe}.csv'
current_date = datetime.datetime.now()
formatted_date = current_date.strftime("%d %b, %Y")
hr = datetime.datetime.now().hour

# Full Cycle
# date_from = "12 May, 2020"
date_from = "1 Jan, 2023"
date_to = formatted_date

# Worst Time
# date_from = "1 Jan, 2022"
# date_to = "12 May, 2023"

param_list = [
    {'symbol':'ROSEUSDT', 'param':(800, 2000, 0.25), 'order_size':170}, # AR 1.57, SHARPE 1.82
    {'symbol':'ILVUSDT' , 'param':(1500, 3400, 0), 'order_size':0.3}, # AR 1.73, SHARPE 2.17
    {'symbol':'AVAXUSDT', 'param':(400, 3400, 0), 'order_size':0.6}, # AR 1.6, SHARPE 1.89
    {'symbol':'BAKEUSDT', 'param':(100, 3800, 0.5), 'order_size':65}, # AR 1.73, SHARPE 1.99
    {'symbol':'GALAUSDT', 'param':(300, 2100, 0.75), 'order_size':950}, # AR 1.57, SHARPE 2.16
    {'symbol':'OPUSDT',   'param':(500, 400, 0), 'order_size':8}, # AR 1.43, SHARPE 2.63
    {'symbol':'ARBUSDT' , 'param':(1400, 3300, 0), 'order_size':12}, # AR 1.44, SHARPE 2.28
    {'symbol':'SUIUSDT' , 'param':(1500, 200, 0.25), 'order_size':14}, # AR 1.35, SHARPE 2.1
    {'symbol':'NEARUSDT', 'param':(800, 2100, 0), 'order_size':8}, # AR 1.64, SHARPE 1.9
    {'symbol':'MATICUSDT','param':(200, 600, 0.5), 'order_size':25}, # AR 1.05, SHARPE 1.51
    {'symbol':'ALGOUSDT', 'param':(700, 2500, 0.25), 'order_size':125}, # AR 1.01, SHARPE 1.5
    {'symbol':'SOLUSDT' , 'param':(100, 600, 0.25), 'order_size':0.2}, # AR 1.61, SHARPE 1.93
    {'symbol':'LINAUSDT', 'param':(100, 3000, 1.5), 'order_size':2600}, # AR 1.57, SHARPE 2.47
    {'symbol':'RUNEUSDT', 'param':(100, 2300, 0.25), 'order_size':5}, # AR 1.47, SHARPE 2.06
    {'symbol':'ONEUSDT' , 'param':(1700, 1500, 0), 'order_size':140}, # AR 1, SHARPE 1.84
    {'symbol':'1INCHUSDT','param':(2400, 3100, 1), 'order_size':50}, # AR 1.35, SHARPE 2.1
    {'symbol':'RNDRUSDT', 'param':(2800, 1300, 0), 'order_size':4}, # AR 1.73, SHARPE 1.46
    {'symbol':'FETUSDT' , 'param':(4600, 1700, 0.25), 'order_size':35}, # AR 1.06, SHARPE 1.87
    {'symbol':'ADAUSDT' , 'param':(400, 800, 1), 'order_size':40}, # AR 1.02, SHARPE 1.98 > try 300, 700, 0.75
    {'symbol':'SANDUSDT', 'param':(2800, 500, 0), 'order_size':50}, # AR 0.83, SHARPE 1.98 > try 2800, 500, 0
    {'symbol':'FILUSDT' , 'param':(1300, 100, 1), 'order_size':4}, # AR 0.94, SHARPE 1.57
    {'symbol':'DOTUSDT' , 'param':(800, 3200, 0.25), 'order_size':3}, # AR 1.06, SHARPE 1.4
    {'symbol':'APTUSDT' , 'param':(200, 1200, 0.5), 'order_size':2}, # AR 1.45, SHARPE 2.01 / RARE TRADING CHANCE
    {'symbol':'IMXUSDT' , 'param':(4000, 1400, 0), 'order_size':10}, # AR 1.04, SHARPE 1.45
    {'symbol':'MANAUSDT', 'param':(800, 1100, 0), 'order_size':50}, # AR 0.98, SHARPE 1.45
    {'symbol':'WOOUSDT' , 'param':(100, 400, 1), 'order_size':65}, # AR 0.98, SHARPE 1.45
    {'symbol':'ICPUSDT' , 'param':(800, 1900, 0), 'order_size':1.5}, # AR 1.22, SHARPE 1.39
    {'symbol':'COMPUSDT', 'param':(100, 1300, 1.5), 'order_size':0.4}, # AR 0.78, SHARPE 1.66
    {'symbol':'CHZUSDT' , 'param':(1500, 3300, 0.5), 'order_size':200}, # AR 0.8, SHARPE 1.22
    {'symbol':'ETCUSDT',  'param':(900, 2100, 3), 'order_size':1},  # AR 0.51, SHARPE 1.82 / RARE TRADING CHANCE

    # {'symbol':'FLOWUSDT', 'param':(700, 900, 1.5)}, # AR 1.05, SHARPE 1.51
    # {'BTCUSDT': (1300, 1600, 0)}, # AR 0.62, SHARPE 1.2
    # {'EOSUSDT': (200, 2100, 2)}, # AR 0.57, SHARPE 1.02

    # {'SEIUSDT': (2400, 2600, 0.25)}, # AR 3.83, SHARPE 3.88 / NEW LISTED
    # {'BLURUSDT': (2400, 800, 0.5)}, # AR 2.2, SHARPE 3.42 / NEW LISTED, NEED MORE DATA

]



def fetch_data(symbol, timeframe):
    if timeframe == '15min':
        klines_15min = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_15MINUTE, date_from, date_to)
        return klines_15min

    if timeframe == '1h':
        klines_1h = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1HOUR, date_from, date_to)
        return klines_1h

    if timeframe == '4h':
        klines_4h = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_4HOUR, date_from, date_to)
        return klines_4h

    if timeframe == '1d':
        klines_1d = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1DAY, date_from, date_to)
        return klines_1d


def form_data_frame(symbol, timeframe):
    df = fetch_data(symbol, timeframe=timeframe)  # 15min / 1h / 4h / 1d
    col = ['Date', 'open', 'high', 'low', 'close', 'volume', 'close time', 'Quote asset volume', 'Num trades',
           'buy vol', 'buy asset volume', 'ignore.']
    df = pd.DataFrame(df, columns=col)
    df['Date'] = pd.to_datetime(df['Date'], unit='ms')
    df['Date'] = df['Date'].dt.strftime('%d/%m/%Y %H:%M')
    df['close'] = pd.to_numeric(df['close'], errors='coerce')

    # Fetch new data
    data = bybit_request.get_kline('linear', symbol, '60', limit=hr)
    data = data['list']
    col = ['Date', 'open', 'high', 'low', 'close', 'volume', 'turnover']
    df2 = pd.DataFrame(data=data, columns=col)
    df2['Date'] = pd.to_datetime(df2['Date'], unit='ms')
    df2['Date'] = df2['Date'].dt.strftime('%d/%m/%Y %H:%M')

    # Reverse the DataFrame to have the latest data at the bottom
    df2 = df2.iloc[::-1].reset_index(drop=True)

    # Align df2 to have the same columns as df, with NaNs where data is not available
    df2 = df2.reindex(columns=df.columns)

    # Now that both DataFrames are aligned, concatenate them
    df = pd.concat([df, df2], ignore_index=True)

    df = df[['Date', 'close']]

    return df


def create_merged_csv(csv_name):
    def get_oi_data(pages):
        data_list = []
        cursor = ''
        for _ in range(pages):
            response = bybit_request.get_open_interest(category=category, symbol=symbol, interval_time=timeframe, limit=limit, cursor=cursor)
            for i in response['list']:
                data_list.append(i)

            if 'nextPageCursor' in response:
                cursor = response['nextPageCursor']
            else:
                break

        print('Total rows:', len(data_list))

        col = ['timestamp', 'openInterest']
        df = pd.DataFrame(data_list, columns=col)
        df = df.rename(columns={'timestamp':'Date'})
        df['Date'] = pd.to_datetime(df['Date'], unit='ms').dt.round('H')

        return df


    df2 = form_data_frame(symbol, timeframe)

    num_rows = len(df2)
    pages = int(round((num_rows / 200), 0))

    df = get_oi_data(pages)

    df['Date'] = pd.to_datetime(df['Date'], format='%d/%m/%Y %H:%M').dt.round('H')
    df2['Date'] = pd.to_datetime(df2['Date'], format='%d/%m/%Y %H:%M').dt.round('H')

    merge_df = pd.merge(df, df2, on='Date')
    merge_df = merge_df.sort_values(ascending=True, by='Date').reset_index(drop=True)
    merge_df = merge_df.drop_duplicates(subset='Date', keep='first')
    merge_df.to_csv(csv_name, index=False)

    return merge_df



def update_merge_csv():
    csv_name = f'merge_{symbol}_{timeframe}.csv'
    df = pd.read_csv(csv_name)

    data = bybit_request.get_kline('linear', symbol, '5', limit=1)
    data = data['list']
    col = ['Date', 'open', 'high', 'low', 'close', 'volume', 'turnover']
    df2 = pd.DataFrame(data=data, columns=col)
    df2['Date'] = pd.to_datetime(df2['Date'], unit='ms').dt.round('H')
    # df2['Date'] = df2['Date'].dt.strftime('%d/%m/%Y %H:%M')
    # df2['Date'] = pd.to_datetime(df2['Date'], format='%d/%m/%Y %H:%M')

    oi = bybit_request.get_open_interest(category=category, symbol=symbol, interval_time='1h', limit=1, cursor=None)
    oi = oi['list']
    col2 = ['openInterest', 'timestamp']
    df3 = pd.DataFrame(data=oi, columns=col2)
    df3 = df3.rename(columns={'timestamp':'Date'})
    df3['Date'] = pd.to_datetime(df3['Date'], unit='ms').dt.round('H')

    df2['Date'] = pd.to_datetime(df2['Date'])  # This will automatically parse the ISO format
    df3['Date'] = pd.to_datetime(df3['Date'])

    merged_df = pd.merge(df2, df3, on='Date')
    merged_df = merged_df[['Date', 'openInterest', 'close']]


    # Now that both DataFrames are aligned, concatenate them
    df = pd.concat([df, merged_df], ignore_index=True)

    # Save the final DataFrame to CSV
    df.to_csv(csv_name, mode='w', index=False, header=True)

    print(f'CSV {csv_name} updated')

    return df


def bband(df, n, x, y):
    df['close'] = pd.to_numeric(df['close'], errors='coerce')
    df['pct_change'] = df['close'].pct_change()
    df['ma_price'] = df['close'].rolling(n).mean()

    df['openInterest'] = pd.to_numeric(df['openInterest'])
    df['ma'] = df['openInterest'].rolling(x).mean()
    df['sd'] = df['openInterest'].rolling(x).std()
    df['z'] = (df['openInterest'] - df['ma']) / df['sd']
    df['+b'] = df['ma'] + (df['sd'] * y)
    df['-b'] = df['ma'] - (df['sd'] * y)

    # Open long if z > y and close > ma_price
    long_cond = (df['z'] > y) & ((df['close'] > df['ma_price']))

    # Open short if z > y and close < ma_price
    short_cond = (df['z'] > y) & ((df['close'] < df['ma_price']))

    # Hold if within +/- y band
    hold_cond = (np.abs(df['z']) <= y)

    df['pos'] = 0

    df['pos'] = np.where(long_cond, 1,
                         np.where(short_cond, -1,
                                  np.where(hold_cond, 0, df['pos'])))

    currrent_pos = df['pos'].iloc[-1]
    previous_pos = df['pos'].iloc[-2]

    return currrent_pos, previous_pos, df


def position_count():
    position = bybit_request.get_position_info(category=category, settleCoin='USDT')
    position_list = position['result']['list']
    total_position = len(position_list)

    return total_position


while True:
    current_time = datetime.datetime.now()
    if current_time.minute == 0 and current_time.second <= 5:
        time.sleep(120)
        try:
            for pairs in param_list:
                symbol = pairs['symbol']
                param = pairs['param']
                order_size = pairs['order_size']
                csv_name = f'merge_{symbol}_{timeframe}.csv'
                if not os.path.isfile(csv_name):
                    df = create_merged_csv(csv_name)
                else:
                    print('use existing csv')
                    df = update_merge_csv()
                    time.sleep(2)
                n, x, y = param
                currrent_pos, previous_pos, bband_df = bband(df, n, x, y)

                position = bybit_request.get_position_info(category=category, symbol=symbol)
                info = position['result']['list'][0]
                side = info['side'] # Buy, Sell, None

                if side == 'Sell':
                    existing_pos = -1
                elif side == 'Buy':
                    existing_pos = 1
                else:
                    existing_pos = 0

                total_position = position_count()
                print(bband_df.tail(3))
                print(f'Total Position = {total_position}')
                print(f'existing_pos = {existing_pos}, pos = {currrent_pos}')

                if currrent_pos != previous_pos: # aviod delayed pos after reboot
                    if existing_pos != currrent_pos: # check if still have enough balance for sending order
                        net_pos = currrent_pos - existing_pos
                        bid, ask = bybit_request.fetch_bybit_data(symbol=symbol, category='linear')
                        size = abs(order_size * net_pos)
                        print(f'{symbol} pos = {currrent_pos}')
                        if net_pos > 0:
                            print(f'sending {symbol} {currrent_pos} buy order')
                            response = bybit_request.place_market_order(category='linear', side='Buy', symbol=symbol,
                                                                        order_type='Market', qty=size)
                            message = f'OI Trade\nBybit Long {symbol}\nPos = {currrent_pos}\nRef Price = {ask}\nsize = {size}({net_pos})\n\n{response})'
                            print(message)
                            support_function.tg_pop(message)
                        if net_pos < 0:
                            print(f'sending {symbol} {currrent_pos} sell order')
                            response = bybit_request.place_market_order(category='linear', side='Sell', symbol=symbol,
                                                                        order_type='Market', qty=size)
                            message = f'OI Trade\nBybit Short {symbol}\nPos = {currrent_pos}\nRef Price = {ask}\nsize = {size}({net_pos})\n\n{response})'
                            print(message)
                            support_function.tg_pop(message)
                print('========================== Next Symbol ==========================')
            print('========================== Loop End ==========================')

        except requests.exceptions.RequestException as e:
            support_function.tg_pop('OI Trade Data Gathering ConnectionResetError Occurs, reboot now')
            continue

        except Exception as e:
            support_function.tg_pop(str(e))
            print(e)
            continue

        total_position = position_count()
        support_function.tg_pop(f'OI Trade Total Position = {total_position}')
        # Calculate how many seconds to sleep until the start of the next hour
        current_time = datetime.datetime.now()  # Update current_time after processing is complete
        time_to_sleep = (3600 - current_time.minute * 60 - current_time.second)
        time.sleep(time_to_sleep)
    else:
        # Calculate how many seconds to sleep until the start of the next hour
        time_to_sleep = (3600 - current_time.minute * 60 - current_time.second)
        time.sleep(time_to_sleep)

