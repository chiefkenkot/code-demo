import sys

import pandas as pd
import numpy as np
import json
import uuid
import hmac
import hashlib
import urllib
import requests
from pprint import pprint
from datetime import datetime
import os
import time
import asyncio
import base64
import pal
from CEX_Request_V3 import HashKeyRequest, DeribitRequest
import traceback


pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', 1000)

hashkey_request = HashKeyRequest(pal.hashkey_key, pal.hashkey_secret)
deribit_request = DeribitRequest(pal.deribit_client_id, pal.deribit_secret)


###### Create CSV ######

def fetch_deribit_data(symbol):
    url = f'https://www.deribit.com/api/v2/public/get_order_book?instrument_name={symbol}&depth=1'
    response = requests.get(url)
    data = response.json()

    ask_price = float(data['result']['asks'][0][0])
    bid_price = float(data['result']['bids'][0][0])

    return ask_price, bid_price



def fetch_hashkey_data(symbol, limit=5):
    base_url = "https://api-pro.hashkey.com/quote/v1/depth"
    params = {"symbol": symbol, "limit": limit}

    response = requests.get(base_url, params=params)

    if response.status_code == 200:
        response = response.json()
        ask_price = float(response['a'][0][0])
        bid_price = float(response['b'][0][0])
        return ask_price, bid_price
    else:
        print(f"Failed to fetch data: {response.status_code}")
        return None


def price_diff_rtq():
    # RTQ
    deribit_ask_price, deribit_bid_price = fetch_deribit_data(deribit_symbol)
    hashkey_ask_price, hashkey_bid_price = fetch_hashkey_data(hashkey_symbol)
    ask_diff_now = round((deribit_ask_price - float(hashkey_ask_price)), 2)
    bid_diff_now = round((deribit_bid_price - float(hashkey_bid_price)), 2)

    return ask_diff_now, bid_diff_now, deribit_ask_price, hashkey_ask_price


def fetch_and_append_data_merged(interval):
    deribit_pair = 'BTC-PERPETUAL'
    deribit_ask_price, deribit_bid_price = fetch_deribit_data(deribit_pair)
    deribit_ask_header = 'Deribit_' + deribit_pair.replace('/', '') + '_Ask'
    deribit_bid_header = 'Deribit_' + deribit_pair.replace('/', '') + '_Bid'

    hashkey_pair = 'BTCUSD'
    hashkey_ask_price, hashkey_bid_price = fetch_hashkey_data(hashkey_pair)
    hashkey_ask_header = 'Hashkey_' + hashkey_pair + '_Ask'
    hashkey_bid_header = 'Hashkey_' + hashkey_pair + '_Bid'

    data_dict = {
        'timestamp': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        deribit_ask_header: [deribit_ask_price],
        deribit_bid_header: [deribit_bid_price],
        hashkey_ask_header: [hashkey_ask_price],
        hashkey_bid_header: [hashkey_bid_price],
    }

    # Create a DataFrame using the data dictionary
    df = pd.DataFrame(data_dict)

    # Append the DataFrame to a CSV file
    csv_file = f"BTC_Deribit-Hashkey_Data_{interval} Sec.csv"

    # Check if the file exists, if not, create it with headers
    if not os.path.isfile(csv_file):
        df.to_csv(csv_file, mode="w", header=True, index=False)
    else:
        df.to_csv(csv_file, mode="a", header=False, index=False)

    # print(df)

    return data_dict





def tg_pop(e):

    base_url = 'https://api.telegram.org/bot5366919147:AAHXp_HHe2o38FBiahz8PspWah6IYHOXbJ8/sendMessage?chat_id=-&text='

    message = (e)

    requests.get(base_url+message)





def base(x, y, cancel_counter, order_counter, interval):

    start_time = time.time()
    print(f'start time = {start_time}')
    ask_df = pd.read_csv(f'{ticker}_Deribit-Hashkey_Data_{interval} Sec.csv')
    ask_df= ask_df[['timestamp', f'Deribit_{ticker}-PERPETUAL_Ask', f'Hashkey_{ticker}USD_Ask']]
    ask_df['ask_diff'] = ask_df[f'Deribit_{ticker}-PERPETUAL_Ask'] - ask_df[f'Hashkey_{ticker}USD_Ask']
    ask_df['ask_ma'] = ask_df['ask_diff'].rolling(x).mean()
    ask_df['ask_sd'] = ask_df['ask_diff'].rolling(x).std()
    ask_df['ask_z'] = (ask_df['ask_diff'] - ask_df['ask_ma']) / ask_df['ask_sd']
    ask_df['ask_+b'] = ask_df['ask_ma'] + ask_df['ask_sd'] * y
    ask_df['ask_-b'] = ask_df['ask_ma'] - ask_df['ask_sd'] * y
    ask_df['ask_pos'] = np.where(ask_df['ask_z'] > y, -1, np.where(ask_df['ask_z'] < -y, 1, 0))
    # ask_df['ask_pos'] = np.where(ask_df['ask_diff'] > ask_df['ask_ma'], 1,
    #                              np.where(ask_df['ask_diff'] < ask_df['ask_ma'], -1, 0))

    bid_df = pd.read_csv(f'{ticker}_Deribit-Hashkey_Data_{interval} Sec.csv')
    bid_df = bid_df[['timestamp', f'Deribit_{ticker}-PERPETUAL_Bid',  f'Hashkey_{ticker}USD_Bid']]
    bid_df['bid_diff'] = bid_df[f'Deribit_{ticker}-PERPETUAL_Bid'] - bid_df[f'Hashkey_{ticker}USD_Bid']
    bid_df['bid_ma'] = bid_df['bid_diff'].rolling(x).mean()
    bid_df['bid_sd'] = bid_df['bid_diff'].rolling(x).std()
    bid_df['bid_z'] = (bid_df['bid_diff'] - bid_df['bid_ma']) / bid_df['bid_sd']
    bid_df['bid_+b'] = bid_df['bid_ma'] + bid_df['bid_sd'] * y
    bid_df['bid_-b'] = bid_df['bid_ma'] - bid_df['bid_sd'] * y
    bid_df['bid_pos'] = np.where(bid_df['bid_z'] > y, -1, np.where(bid_df['bid_z'] < -y, 1, 0))

    pos = ask_df['ask_pos'].iloc[-1]
    ma = ask_df['ask_ma'].iloc[-1]
    # diff1 = float(bid_df['bid_+b'].iloc[-1])
    # diff2 = float(bid_df['bid_-b'].iloc[-1])
    # diff3 = float(ask_df['ask_+b'].iloc[-1])
    diff2 = float(bid_df['bid_ma'].iloc[-1])
    diff3 = float(ask_df['ask_ma'].iloc[-1])

    # diff4 = float(ask_df['ask_-b'].iloc[-1])
    # print(type(pos)) #<class 'numpy.int32'>
    # print(pos)
    print('======================================')
    print(ask_df.tail(3))
    print('======================================')
    print(bid_df.tail(3))
    print('======================================')

    # fetch Deribit / Hashkey position


    response, direction, size = deribit_request.get_position(deribit_symbol)
    position, balance = deribit_request.get_account_summary('BTC')
    print(f'Deribit position: symbol = {deribit_symbol}, direction = {direction}, size = {size}\nspot balance = {balance}')

    response, balance = hashkey_request.get_account_info(1497220482998621184)
    pprint(f'Hashkey balance = {balance}')

    print('======================================')


    def transactions_data(pos, deribit_transaction, hashkey_transaction):
        data = {
            'timestamp': [datetime.now().strftime('%Y-%m-%d %H:%M:%S')],
            'Deribit': [deribit_transaction],
            'Hashkey': [hashkey_transaction],
            'side': [pos],
            'ma':[ma]
        }
        df = pd.DataFrame(data)

        csv_file = f"Result_{ticker}-Deribit-Hashkey_{interval} Sec.csv"

        if not os.path.isfile(csv_file):
            df.to_csv(csv_file, mode="w", header=True, index=False)
        else:
            df.to_csv(csv_file, mode="a", header=False, index=False)

    ###### place order ######
    # try:
    if pos == -1: # Deribit > Hashkey
        # if previously == 1
        if direction == 'buy':
            deribit_ask_price, deribit_bid_price = fetch_deribit_data(deribit_symbol)
            response, deribit_order_id, deribit_order_price = deribit_request.place_sell_order(
                instrument_name=deribit_symbol,
                amount= usd*2,
                price= deribit_ask_price,
                post_only=True,
                order_type='limit')
            print('signal = -1, position = 1, Short Deribt, Long Hashkey')
            hashkey_amount = round(usd / deribit_order_price, 5)
            start_time = time.time()
            while True:
                try:
                    elapsed_time = time.time() - start_time
                    ask_diff_now, bid_diff_now, deribit_ask_price, hashkey_ask_price = price_diff_rtq()
                    if elapsed_time <= (interval-3):
                        if ask_diff_now > diff3: # check if the spread is still wide enough

                            # Check if order is executed
                            response, order_state = deribit_request.get_order_state(order_id=deribit_order_id)
                            print(f'state = {order_state}')
                            if order_state in ['open']:
                                print(f'has open order:{deribit_order_id}')
                            else:
                                response, hashkey_orderId, hashkey_clientOrderId = hashkey_request.create_order(
                                    symbol=hashkey_symbol,
                                    side='BUY',
                                    order_type='MARKET',
                                    quantity=usd*2)

                                response, avg_price = hashkey_request.query_order(hashkey_orderId, hashkey_clientOrderId)

                                order_counter += 1
                                print(f'Hashkey Market Buy @ {avg_price}, Response = {response}')
                                transactions_data(pos=pos, deribit_transaction=deribit_order_price,
                                                  hashkey_transaction=avg_price)
                                break
                            time.sleep(time_sleep)

                        elif ask_diff_now <= diff3:
                            ##### debug purpose
                            response, order_state = deribit_request.get_order_state(order_id=deribit_order_id)
                            print(f'state before cancel = {order_state}')
                            #####
                            response, cancel_status = deribit_request.cancel_order(order_id=deribit_order_id)
                            print(f'Deribit cancel status = {cancel_status}')
                            if cancel_status != 'cancelled':
                                response, hashkey_orderId, hashkey_clientOrderId = hashkey_request.create_order(
                                    symbol=hashkey_symbol,
                                    side='BUY',
                                    order_type='MARKET',
                                    quantity=usd*2)

                                response, avg_price = hashkey_request.query_order(hashkey_orderId,
                                                                                  hashkey_clientOrderId)

                                order_counter += 1
                                print(f'Hashkey Market Buy @ {avg_price}, Response = {response}')
                                transactions_data(pos=pos, deribit_transaction=deribit_order_price,
                                                  hashkey_transaction=avg_price)
                            else:
                                cancel_counter += 1
                            print(f'cancel reason = ask_diff_now {ask_diff_now} <= ask+b {diff3}')
                            break

                    elif elapsed_time > (interval - 4):
                        ##### debug purpose
                        response, order_state = deribit_request.get_order_state(order_id=deribit_order_id)
                        print(f'state before cancel = {order_state}')
                        #####
                        response, cancel_status = deribit_request.cancel_order(order_id=deribit_order_id)
                        print(f'Deribit cancel status = {cancel_status}')
                        if cancel_status != 'cancelled':
                            response, hashkey_orderId, hashkey_clientOrderId = hashkey_request.create_order(
                                symbol=hashkey_symbol,
                                side='BUY',
                                order_type='MARKET',
                                quantity=usd*2)

                            response, avg_price = hashkey_request.query_order(hashkey_orderId,
                                                                              hashkey_clientOrderId)

                            order_counter += 1
                            print(f'Hashkey Market Buy @ {avg_price}, Response = {response}')
                            transactions_data(pos=pos, deribit_transaction=deribit_order_price,
                                              hashkey_transaction=avg_price)
                        else:
                            cancel_counter += 1
                        print(f'cancel reason: loop elasped time > 12 ({elapsed_time})')
                        break

                except requests.exceptions.Timeout:
                    print("Request timed out. Retrying...")
                    continue

                except Exception as e:
                    print(f'Error: {e}')
                    print(traceback.format_exc())
                    tg_pop(f'Deribit / Hashkey {ticker} error: {e},\n traceback: {traceback.format_exc()}')
                    print('pos == -1, if dead')
                    sys.exit(1)

        # if previously == -1, pass
        elif direction == 'sell':
            print('signal = -1, position = -1, keep existing position')

        # if there is no position, open new position
        else:
            deribit_ask_price, deribit_bid_price = fetch_deribit_data(deribit_symbol)
            response, deribit_order_id, deribit_order_price = deribit_request.place_sell_order(
                instrument_name=deribit_symbol,
                amount=usd,
                price=deribit_ask_price,
                post_only=True,
                order_type='limit')
            print('signal = -1, position = 0, Short Deribt, Long Hashkey')
            hashkey_amount = round(usd / deribit_order_price, 5)
            start_time = time.time()
            while True:
                try:
                    elapsed_time = time.time() - start_time
                    ask_diff_now, bid_diff_now, deribit_ask_price, hashkey_ask_price = price_diff_rtq()
                    if elapsed_time <= (interval - 3): # check if the order can execute within certain time frame
                        if ask_diff_now > diff3:  # check if the spread is still wide enough
                            # Check if order is executed
                            response, order_state = deribit_request.get_order_state(order_id=deribit_order_id)
                            print(f'state = {order_state}')
                            if order_state in ['open']:
                                print(f'has open order:{deribit_order_id}')
                            else:
                                response, hashkey_orderId, hashkey_clientOrderId = hashkey_request.create_order(
                                    symbol=hashkey_symbol,
                                    side='BUY',
                                    order_type='MARKET',
                                    quantity=usd)

                                response, avg_price = hashkey_request.query_order(hashkey_orderId,
                                                                                  hashkey_clientOrderId)

                                order_counter += 1
                                print(f'Hashkey Market Buy @ {avg_price}, Response = {response}')
                                transactions_data(pos=pos, deribit_transaction=deribit_order_price,
                                                  hashkey_transaction=avg_price)
                                break
                            time.sleep(time_sleep)

                        elif ask_diff_now <= diff3:
                            ##### debug purpose
                            response, order_state = deribit_request.get_order_state(order_id=deribit_order_id)
                            print(f'state before cancel = {order_state}')
                            #####
                            response, cancel_status = deribit_request.cancel_order(order_id=deribit_order_id)
                            print(f'Deribit cancel status = {cancel_status}')
                            if cancel_status != 'cancelled':
                                response, hashkey_orderId, hashkey_clientOrderId = hashkey_request.create_order(
                                    symbol=hashkey_symbol,
                                    side='BUY',
                                    order_type='MARKET',
                                    quantity=usd)

                                response, avg_price = hashkey_request.query_order(hashkey_orderId,
                                                                                  hashkey_clientOrderId)

                                order_counter += 1
                                print(f'Hashkey Market Buy: {response}, Transaction price = {avg_price}')
                                transactions_data(pos=pos, deribit_transaction=deribit_order_price,
                                                  hashkey_transaction=avg_price)
                            else:
                                cancel_counter += 1
                            print(f'cancel reason = ask_diff_now {ask_diff_now} <= ask+b {diff3}')
                            break

                    elif elapsed_time > (interval - 4):
                        ##### debug purpose
                        response, order_state = deribit_request.get_order_state(order_id=deribit_order_id)
                        print(f'state before cancel = {order_state}')
                        #####
                        response, cancel_status = deribit_request.cancel_order(order_id=deribit_order_id)
                        print(f'Deribit cancel status = {cancel_status}')
                        if cancel_status != 'cancelled':
                            response, hashkey_orderId, hashkey_clientOrderId = hashkey_request.create_order(
                                symbol=hashkey_symbol,
                                side='BUY',
                                order_type='MARKET',
                                quantity=usd)

                            response, avg_price = hashkey_request.query_order(hashkey_orderId,
                                                                              hashkey_clientOrderId)

                            order_counter += 1
                            print(f'Hashkey Market Buy: {response}, Transaction price = {avg_price}')
                            transactions_data(pos=pos, deribit_transaction=deribit_order_price,
                                              hashkey_transaction=avg_price)
                        else:
                            cancel_counter += 1
                        print(f'cancel reason: loop elasped time > 12 ({elapsed_time})')
                        break

                except requests.exceptions.Timeout:
                    print("Request timed out. Retrying...")
                    continue

                except Exception as e:
                    print(f'Error: {e}')
                    print(traceback.format_exc())
                    tg_pop(f'Deribit / Hashkey {ticker} error: {e},\n traceback: {traceback.format_exc()}')
                    print('pos == -1, else dead')
                    sys.exit(1)

    if pos == 1:
        # if previously == -1
        if direction == 'sell':
            deribit_ask_price, deribit_bid_price = fetch_deribit_data(deribit_symbol)
            response, deribit_order_id, deribit_order_price = deribit_request.place_buy_order(
                instrument_name=deribit_symbol,
                amount=usd * 2,
                price=deribit_bid_price,
                post_only=True,
                order_type='limit')
            print('signal = 1, position = -1, Long Deribt, Short Hashkey')
            hashkey_amount = round(usd / deribit_order_price, 5)
            start_time = time.time()
            while True:
                try:
                    elapsed_time = time.time() - start_time
                    ask_diff_now, bid_diff_now, deribit_ask_price, hashkey_ask_price = price_diff_rtq()
                    if elapsed_time <= (interval - 3):  # check if the order can execute within certain time frame
                        if bid_diff_now < diff2:  # check if the spread is still wide enough
                            # Check if order is executed
                            response, order_state = deribit_request.get_order_state(order_id=deribit_order_id)
                            print(f'state = {order_state}')
                            if order_state in ['open']:
                                print(f'has open order:{deribit_order_id}')
                            else:
                                response, hashkey_orderId, hashkey_clientOrderId = hashkey_request.create_order(
                                    symbol=hashkey_symbol,
                                    side='SELL',
                                    order_type='MARKET',
                                    quantity=hashkey_amount*2)

                                response, avg_price = hashkey_request.query_order(hashkey_orderId,
                                                                                  hashkey_clientOrderId)

                                order_counter += 1
                                print(f'Hashkey Market Sell @ {avg_price}, Response = {response}')
                                transactions_data(pos=pos, deribit_transaction=deribit_order_price,
                                                  hashkey_transaction=avg_price)
                                break
                            time.sleep(time_sleep)

                        elif bid_diff_now >= diff2:
                            ##### debug purpose
                            response, order_state = deribit_request.get_order_state(order_id=deribit_order_id)
                            print(f'state before cancel = {order_state}')
                            #####
                            response, cancel_status = deribit_request.cancel_order(order_id=deribit_order_id)
                            print(f'Deribit cancel status = {cancel_status}')
                            if cancel_status != 'cancelled':
                                response, hashkey_orderId, hashkey_clientOrderId = hashkey_request.create_order(
                                    symbol=hashkey_symbol,
                                    side='SELL',
                                    order_type='MARKET',
                                    quantity=hashkey_amount*2)

                                response, avg_price = hashkey_request.query_order(hashkey_orderId,
                                                                                  hashkey_clientOrderId)

                                order_counter += 1
                                print(f'Hashkey Market Sell @ {avg_price}, Response = {response}')
                                transactions_data(pos=pos, deribit_transaction=deribit_order_price,
                                                  hashkey_transaction=avg_price)
                            else:
                                cancel_counter += 1
                            print(f'cancel reason = bid_diff_now {bid_diff_now} >= bid-b {diff2}')
                            break

                    elif elapsed_time > (interval - 4):
                        ##### debug purpose
                        response, order_state = deribit_request.get_order_state(order_id=deribit_order_id)
                        print(f'state before cancel = {order_state}')
                        #####
                        response, cancel_status = deribit_request.cancel_order(order_id=deribit_order_id)
                        print(f'Deribit cancel status = {cancel_status}')
                        if cancel_status != 'cancelled':
                            response, hashkey_orderId, hashkey_clientOrderId = hashkey_request.create_order(
                                symbol=hashkey_symbol,
                                side='SELL',
                                order_type='MARKET',
                                quantity=hashkey_amount*2)

                            response, avg_price = hashkey_request.query_order(hashkey_orderId,
                                                                              hashkey_clientOrderId)

                            order_counter += 1
                            print(f'Hashkey Market Sell @ {avg_price}, Response = {response}')
                            transactions_data(pos=pos, deribit_transaction=deribit_order_price,
                                              hashkey_transaction=avg_price)
                        else:
                            cancel_counter += 1
                        print(f'cancel reason: loop elasped time > 12 ({elapsed_time})')
                        break

                except requests.exceptions.Timeout:
                    print("Request timed out. Retrying...")
                    continue

                except Exception as e:
                    print(f'Error: {e}')
                    print(traceback.format_exc())
                    tg_pop(f'Deribit / Hashkey {ticker} error: {e},\n traceback: {traceback.format_exc()}')
                    print('pos == 1, if dead')
                    sys.exit(1)

        # if previously == 1, pass
        elif direction == 'buy':
            print('signal = 1, position = 1')


        # if there is no position, open new position
        else:
            deribit_ask_price, deribit_bid_price = fetch_deribit_data(deribit_symbol)
            response, deribit_order_id, deribit_order_price = deribit_request.place_buy_order(
                instrument_name=deribit_symbol,
                amount=usd,
                price=deribit_bid_price,
                post_only=True,
                order_type='limit')
            print('signal = 1, position = 0, Long Deribt, Short Hashkey')
            hashkey_amount = round(usd / deribit_order_price, 5)
            start_time = time.time()
            while True:
                try:
                    elapsed_time = time.time() - start_time
                    ask_diff_now, bid_diff_now, deribit_ask_price, hashkey_ask_price = price_diff_rtq()
                    if elapsed_time <= (interval - 3):  # check if the order can execute within certain time frame
                        if bid_diff_now < diff2:  # check if the spread is still wide enough
                            # Check if order is executed
                            response, order_state = deribit_request.get_order_state(order_id=deribit_order_id)
                            print(f'state = {order_state}')
                            if order_state in ['open']:
                                print(f'has open order:{deribit_order_id}')
                            else:
                                response, hashkey_orderId, hashkey_clientOrderId = hashkey_request.create_order(
                                    symbol=hashkey_symbol,
                                    side='SELL',
                                    order_type='MARKET',
                                    quantity=hashkey_amount)

                                response, avg_price = hashkey_request.query_order(hashkey_orderId,
                                                                                  hashkey_clientOrderId)

                                order_counter += 1
                                print(f'Hashkey Market Sell @ {avg_price}, Response = {response}')
                                transactions_data(pos=pos, deribit_transaction=deribit_order_price,
                                                  hashkey_transaction=avg_price)
                                break
                            time.sleep(time_sleep)

                        elif bid_diff_now >= diff2:
                            ##### debug purpose
                            response, order_state = deribit_request.get_order_state(order_id=deribit_order_id)
                            print(f'state before cancel = {order_state}')
                            #####
                            response, cancel_status = deribit_request.cancel_order(order_id=deribit_order_id)
                            print(f'Deribit cancel status = {cancel_status}')
                            if cancel_status != 'cancelled':
                                response, hashkey_orderId, hashkey_clientOrderId = hashkey_request.create_order(
                                    symbol=hashkey_symbol,
                                    side='SELL',
                                    order_type='MARKET',
                                    quantity=hashkey_amount)

                                response, avg_price = hashkey_request.query_order(hashkey_orderId,
                                                                                  hashkey_clientOrderId)

                                order_counter += 1
                                print(f'Hashkey Market Sell @ {avg_price}, Response = {response}')
                                transactions_data(pos=pos, deribit_transaction=deribit_order_price,
                                                  hashkey_transaction=avg_price)
                            else:
                                cancel_counter += 1
                            print(f'cancel reason = bid_diff_now {bid_diff_now} >= bid-b {diff2}')
                            break

                    elif elapsed_time > (interval - 4):
                        ##### debug purpose
                        response, order_state = deribit_request.get_order_state(order_id=deribit_order_id)
                        print(f'state before cancel = {order_state}')
                        #####
                        response, cancel_status = deribit_request.cancel_order(order_id=deribit_order_id)
                        print(f'Deribit cancel status = {cancel_status}')
                        if cancel_status != 'cancelled':
                            response, hashkey_orderId, hashkey_clientOrderId = hashkey_request.create_order(
                                symbol=hashkey_symbol,
                                side='SELL',
                                order_type='MARKET',
                                quantity=hashkey_amount)

                            response, avg_price = hashkey_request.query_order(hashkey_orderId,
                                                                              hashkey_clientOrderId)

                            order_counter += 1
                            print(f'Hashkey Market Sell @ {avg_price}, Response = {response}')
                            transactions_data(pos=pos, deribit_transaction=deribit_order_price,
                                              hashkey_transaction=avg_price)
                        else:
                            cancel_counter += 1
                        print(f'cancel reason: loop elasped time > 12 ({elapsed_time})')
                        break

                except requests.exceptions.Timeout:
                    print("Request timed out. Retrying...")
                    continue

                except Exception as e:
                    print(f'Error: {e}')
                    print(traceback.format_exc())
                    tg_pop(f'Deribit / Hashkey {ticker} error: {e},\n traceback: {traceback.format_exc()}')
                    print('pos == 1, else dead')
                    sys.exit(1)

    if pos == 0:
        print('signal = 0, pass')


    # except Exception as e:
    #     if "'NoneType' object is not subscriptable" in str(e):
    #         tg_pop(f'Bitget / Bybit BTC error: {e},\n\n , code restarting')
    #     else:
    #         pass


    elapsed = time.time() - start_time
    print(f'elapsed = {elapsed}')
    if elapsed < interval:
        time.sleep(interval - elapsed)
    print('end')

    return order_counter, cancel_counter
    # time.sleep(1234)


###### Deribit Variable ######
ticker = 'BTC'
deribit_symbol = f'{ticker}-PERPETUAL'
# qty = 0.0001
usd = 50

###### Hashkey Variable ######
hashkey_symbol = f'{ticker}USD'

###### Execution ######
counter = 0
cancel_counter = 0
order_counter = 0
interval = 15
time_sleep = 0.1
x = 5
y = 0.4
# size_filter = 20000
# slippage = 0.002

response, balance = deribit_request.get_account_summary('BTC')  # get order position
print(f'Deribit BTC balance = {balance}')

response2, balance = hashkey_request.get_account_info(1497220482998621184)
print(f'Hashkey Balance: {balance}')



# time.sleep(2134)

# bitget_request.get_order_detail(bitget_symbol, 1071697845636714498)

# time.sleep(1234)

while True:
    try:
        if counter < x+1:
            merged_data_dict = fetch_and_append_data_merged(interval=interval)
            counter += 1
            print(f'accumulating data {counter}')
            time.sleep(interval)

        else:
            merged_data_dict = fetch_and_append_data_merged(interval=interval)
            order_counter, cancel_counter = base(x, y, cancel_counter, order_counter, interval)
            print(f'order counter = {order_counter}')
            print(f'cancel counter = {cancel_counter}')


    except ConnectionError as e:
        if 'Connection aborted' in str(e):
            continue
        else:
            raise

    except Exception as e:
        print(f'error: {e}')
        import traceback
        print(traceback.format_exc())
        tg_pop(f'Deribit / Hashkey {ticker} error: {e},\n traceback: {traceback.format_exc()}')
        sys.exit(1)

