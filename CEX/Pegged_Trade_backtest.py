import pandas as pd
import numpy as np
import plotly.express as px
import seaborn as sns
import matplotlib.pyplot as plt
import requests
import time
from binance.client import Client
import pal
import os
from Strategy import strategy

strategy = strategy()

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', 1000)

client = Client(pal.binance_key, pal.binance_secret)

# Lead = BTC,
# Pool 1 = ETH, SOL, BNB, AVAX, DOT, SEI
# Pool 2 = MATIC, ARB, LRC, ZRX, ATOM, APT
# Pool 3 = ETC, ENS, INJ, NEAR, TIA, FIL, SUI, ORDI, EOS, CHZ, BLUR, PYPH


symbol1 = 'OPUSDT'
symbol2 = 'NEARUSDT'

# Full Cycle
date_from = "12 May, 2020"
date_to = "16 Jan, 2024"

# Worst Time
# date_from = "1 Jan, 2022"
# date_to = "12 May, 2023"

timeframe = '1h' # 15min / 1h / 4h / 1d
csv_name_1 = f'{symbol1}_{timeframe}_({date_from}-{date_to}).csv'
csv_name_2 = f'{symbol2}_{timeframe}_({date_from}-{date_to}).csv'

###### Draw Data ######
# Time Param: https://python-binance.readthedocs.io/en/latest/constants.html

def fetch_data(symbol, timeframe):
    if timeframe == '15min':
        klines_15min = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_15MINUTE, date_from, date_to)
        return klines_15min

    if timeframe == '1h':
        klines_1h = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1HOUR, date_from, date_to)
        return klines_1h

    if timeframe == '4h':
        klines_4h = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_4HOUR, date_from, date_to)
        return klines_4h

    if timeframe == '1d':
        klines_1d = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1DAY, date_from, date_to)
        return klines_1d

def form_data_frame(symbol, csv_name, timeframe):
    df = fetch_data(symbol, timeframe=timeframe) # 15min / 1h / 4h / 1d
    col = ['Date', 'open', 'high', 'low', 'close', 'volume', 'close time', 'Quote asset volume', 'Num trades', 'buy vol', 'buy asset volume', 'ignore.']
    df = pd.DataFrame(df, columns=col)
    df['Date'] = pd.to_datetime(df['Date'], unit='ms')
    df['close'] = pd.to_numeric(df['close'], errors='coerce')
    df.to_csv(csv_name, mode='w', index=False, header=True)

def write_csv(symbol, csv_name, timeframe):
    if not os.path.isfile(csv_name):
        print('create new csv')
        form_data_frame(symbol, csv_name, timeframe)
        time.sleep(3)
    else:
        print('use existing csv')
        time.sleep(3)

write_csv(symbol=symbol1, csv_name=csv_name_1, timeframe=timeframe)
write_csv(symbol=symbol2, csv_name=csv_name_2, timeframe=timeframe)



# ######### optimization zone ##########


def optimisation(symbol, strategy_function, csv_name_1, csv_name_2=None):
    if timeframe == '15min':
        ma_list = np.arange(200,5000,200)
        thres_list = np.arange(0,3.75,0.25)

    if timeframe == '1h':
        ma_list = np.arange(100,3000,100)
        thres_list = np.arange(0,3.75,0.25)

    if timeframe == '4h':
        ma_list = np.arange(20,600,20)
        thres_list = np.arange(0,3.75,0.25)

    if timeframe == '1d':
        ma_list = np.arange(5,150,5)
        thres_list = np.arange(0,3.75,0.25)


    result_df = pd.DataFrame(columns=['x','y','sharpe', 'annual_return', 'mdd', 'calmar'])
    if 'cross_asset' in strategy_function.__name__:
        for x in ma_list:
            for y in thres_list:
                stats, _ = strategy_function(x, y, timeframe=timeframe, csv_name_1=csv_name_1,
                                             csv_name_2=csv_name_2)  # change function here
                result_df = result_df._append(stats, ignore_index=True)

        result_df = result_df.sort_values(by='sharpe', ascending=False)
        print(result_df.head(10))
        data_table = result_df.pivot(index='x', columns='y', values='sharpe')
        sns.heatmap(data_table, annot=True, fmt='g', cmap='Greens')
        plt.show()

        x = int(result_df['x'].iloc[0])
        y = float(result_df['y'].iloc[0])

        stats, df = strategy_function(x, y, timeframe=timeframe, csv_name_1=csv_name_1,
                                     csv_name_2=csv_name_2)  # change function here
        fig = px.line(df, x='Date', y=['cumu', 'dd', 'bnh_cumu'],
                      title=f'{symbol} strategy = {strategy_function}, x = {x}, y = {y}')
        fig.show()

    else:
        for x in ma_list:
            for y in thres_list:
                stats, _ = strategy_function(x, y, timeframe=timeframe, csv_name=csv_name_1)  # change function here
                result_df = result_df._append(stats, ignore_index=True)

        result_df = result_df.sort_values(by='sharpe', ascending=False)
        print(result_df.head(10))
        data_table = result_df.pivot(index='x', columns='y', values='sharpe')
        sns.heatmap(data_table, annot=True, fmt='g', cmap='Greens')
        plt.show()

        x = int(result_df['x'].iloc[0])
        y = float(result_df['y'].iloc[0])

        stats, df = strategy_function(x, y, timeframe, csv_name=csv_name_1) # change function here
        fig = px.line(df, x='Date', y=['cumu','dd','bnh_cumu'], title=f'{symbol} strategy = {strategy_function}, x = {x}, y = {y}')
        fig.show()





def hybrid_backtest(x1, y1, x2, y2, csv_name):
    stats, df1 = strategy_function_1(x=x1, y=y1, timeframe=timeframe, csv_name=csv_name)
    print(df1.tail(2000))
    stats, df2 = strategy_function_2(x=x2, y=y2, timeframe=timeframe, csv_name=csv_name)
    print(df2.tail(2000))
    # First, ensure that the 'Date' columns in both dataframes are of the same type.
    df1['Date'] = pd.to_datetime(df1['Date'])
    df2['Date'] = pd.to_datetime(df2['Date'])

    # Now merge the two dataframes on the 'Date' column.
    combined_df = pd.merge(df1[['Date', 'cumu']], df2[['Date', 'cumu']], on='Date', how='outer',
                           suffixes=('_strategy1', '_strategy2'))
    # print(combined_df.tail(200))

    # Forward fill any NaN values that may have been introduced by the merge (this may happen if the strategies do not trade on all the same dates).
    combined_df.fillna(method='ffill', inplace=True)

    # If the dataframes start on different dates, you may want to fill NaN with 0 after a backfill to prevent misleading drops at the start of the combined curve.
    combined_df.fillna(0, inplace=True)

    # Calculate the combined cumulative PnL.
    combined_df['cumu_combined'] = combined_df['cumu_strategy1'] + combined_df['cumu_strategy2']

    # Create the interactive line plot.
    fig = px.line(combined_df, x='Date', y=['cumu_strategy1', 'cumu_strategy2', 'cumu_combined'],
                  labels={'value': 'Cumulative PnL', 'variable': 'Strategy'},
                  title='Combined Strategy Cumulative PnL')

    # Customize the layout of the plot.
    fig.update_layout(
        xaxis_title='Date',
        yaxis_title='Cumulative PnL',
        legend_title='Strategy',
    )

    # Show the figure.
    fig.show()


def cross_asset_hybrid_backtest(x1, y1, x2, y2, csv_name_1, csv_name_2):

    stats, df1 = strategy.cross_asset_bband_momentum(x=x1, y=y1, timeframe=timeframe, csv_name_1=csv_name_1, csv_name_2=csv_name_2)
    # print(df1.tail(2000))
    stats, df2 = strategy.cross_asset_bband_reversion(x=x2, y=y2, timeframe=timeframe, csv_name_1=csv_name_1, csv_name_2=csv_name_2)
    # print(df2.tail(2000))
    # First, ensure that the 'Date' columns in both dataframes are of the same type.
    df1['Date'] = pd.to_datetime(df1['Date'])
    df2['Date'] = pd.to_datetime(df2['Date'])

    # Now merge the two dataframes on the 'Date' column.
    combined_df = pd.merge(
        df1[['Date', 'cumu', 'bnh_cumu']],
        df2[['Date', 'cumu']],
        on='Date',
        how='outer',
        suffixes=('_strategy1', '_strategy2')
    )
    # print(combined_df.tail(200))

    # Forward fill any NaN values that may have been introduced by the merge (this may happen if the strategies do not trade on all the same dates).
    combined_df.fillna(method='ffill', inplace=True)

    # If the dataframes start on different dates, you may want to fill NaN with 0 after a backfill to prevent misleading drops at the start of the combined curve.
    combined_df.fillna(0, inplace=True)

    # Calculate the combined cumulative PnL.
    combined_df['cumu_combined'] = combined_df['cumu_strategy1'] + combined_df['cumu_strategy2']
    combined_df['pnl'] = combined_df['cumu_combined'].diff().fillna(0)
    combined_df['bnh_pnl'] = combined_df['bnh_cumu'].diff().fillna(0)

    df = combined_df
    print(df.tail(100))

    if timeframe == '15min':
        sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365 * 24 * 4)  ## 15m
        sharpe = round(sharpe, 2)
        bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 24 * 4)  ## 15m
        bnh_sharpe = round(bnh_sharpe, 2)
        annual_return = round(df['pnl'].mean() * (365 * 24 * 4), 2)  ## 15m

    elif timeframe == '1h':
        sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365 * 24)
        sharpe = round(sharpe, 2)
        bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 24)
        bnh_sharpe = round(bnh_sharpe, 2)
        annual_return = round(df['pnl'].mean() * (365 * 24), 2)

    elif timeframe == '4h':
        sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365 * 6)
        sharpe = round(sharpe, 2)
        bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 6)
        bnh_sharpe = round(bnh_sharpe, 2)
        annual_return = round(df['pnl'].mean() * (365 * 6), 2)

    elif timeframe == '1d':
        sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365)
        sharpe = round(sharpe, 2)
        bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365)
        bnh_sharpe = round(bnh_sharpe, 2)
        annual_return = round(df['pnl'].mean() * (365), 2)


    df['dd'] = df['cumu_combined'].cummax() - df['cumu_combined']
    mdd = round(df['dd'].max(), 3)

    print(f'sharpe = {sharpe}, bnh_sharpe = {bnh_sharpe}, mdd = {mdd}')

    # Create the interactive line plot.
    fig = px.line(df, x='Date', y=['cumu_strategy1', 'cumu_strategy2', 'cumu_combined', 'bnh_cumu'],
                  labels={'value': 'Cumulative PnL', 'variable': 'Strategy'},
                  title='Combined Strategy Cumulative PnL')


    # Customize the layout of the plot.
    fig.update_layout(
        xaxis_title='Date',
        yaxis_title='Cumulative PnL',
        legend_title='Strategy',
    )

    # Show the figure.
    fig.show()


strategy_function_1 = strategy.bband_momentum
strategy_function_2 = strategy.bband_reversion

# optimisation(symbol1, strategy_function_1, csv_name_1)
# optimisation(symbol1, strategy_function_2, csv_name_1)
# optimisation(symbol1, strategy.cross_asset_bband_momentum, csv_name_1, csv_name_2)
# optimisation(symbol1, strategy.cross_asset_bband_reversion, csv_name_1, csv_name_2)
# hybrid_backtest(x1= 2900, y1=0, x2=100, y2=100, csv_name=csv_name_1)

cross_asset_hybrid_backtest(x1= 300, y1=0.25, x2=2100, y2=3.5, csv_name_1=csv_name_1, csv_name_2=csv_name_2)
# stats, df = strategy.cross_asset_bband_merge_pos(x1= 300, y1=0.25, x2=2100, y2=3.5, csv_name_1=csv_name_1, csv_name_2=csv_name_2, timeframe=timeframe)
# print(df.tail(1000))
#
# # Now, plot the cumulative PnL for the strategy versus buy and hold cumulative PnL
# fig = px.line(df, x='Date', y=['cumu', 'bnh_cumu'],
#               labels={'value': 'Cumulative PnL', 'variable': 'Type'},
#               title='Strategy vs Buy & Hold Cumulative PnL')
#
# # Update the legend title
# fig.update_layout(legend_title_text='PnL Type')

# Show the figure
# fig.show()

# strategy.cross_asset_bband_momentum(20, 0, timeframe, csv_name_1, csv_name_2)


