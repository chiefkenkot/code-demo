import os.path
import time
from binance.client import Client
import pandas as pd
import requests
from pprint import pprint
from CEX_Request_V3 import ByBitRequest, Support_Function
import pal
import plotly.graph_objs as go
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import datetime

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', 1000)

bybit_key = pal.bybit_key
bybit_secret = pal.bybit_secret
bybit_request = ByBitRequest(bybit_key, bybit_secret)

client = Client(pal.binance_key, pal.binance_secret)


category = 'linear'
# symbol = 'RNDRUSDT'
timeframe = '1h'
limit = 200

# csv_name = f'{symbol}_{timeframe}.csv'
current_date = datetime.datetime.now()
formatted_date = current_date.strftime("%d %b, %Y")
# Full Cycle
# date_from = "12 May, 2020"
date_from = "1 Jan, 2023"
date_to = formatted_date

# Worst Time
# date_from = "1 Jan, 2022"
# date_to = "12 May, 2023"

param_list = [
    {'symbol':'ROSEUSDT', 'param':(800, 2000, 0.25)}, # AR 1.57, SHARPE 1.82
    {'symbol':'ILVUSDT' , 'param':(1500, 3400, 0)}, # AR 1.73, SHARPE 2.17
    {'symbol':'AVAXUSDT', 'param':(400, 3400, 0)}, # AR 1.6, SHARPE 1.89
    {'symbol':'BAKEUSDT', 'param':(100, 3800, 0.5)}, # AR 1.73, SHARPE 1.99
    {'symbol':'GALAUSDT', 'param':(300, 2100, 0.75)}, # AR 1.57, SHARPE 2.16
    {'symbol':'OPUSDT',   'param':(500, 400, 0)}, # AR 1.43, SHARPE 2.63
    {'symbol':'ARBUSDT' , 'param':(1400, 3300, 0)}, # AR 1.44, SHARPE 2.28
    {'symbol':'SUIUSDT' , 'param':(1500, 200, 0.25)}, # AR 1.35, SHARPE 2.1
    {'symbol':'NEARUSDT', 'param':(800, 2100, 0)}, # AR 1.64, SHARPE 1.9
    {'symbol':'MATICUSDT','param':(200, 600, 0.5)}, # AR 1.05, SHARPE 1.51
    {'symbol':'ALGOUSDT', 'param':(700, 2500, 0.25)}, # AR 1.01, SHARPE 1.5
    {'symbol':'SOLUSDT' , 'param':(100, 600, 0.25)}, # AR 1.61, SHARPE 1.93
    {'symbol':'LINAUSDT', 'param':(100, 3000, 1.5)}, # AR 1.57, SHARPE 2.47
    {'symbol':'RUNEUSDT', 'param':(100, 2300, 0.25)}, # AR 1.47, SHARPE 2.06
    {'symbol':'ONEUSDT' , 'param':(1700, 1500, 0)}, # AR 1, SHARPE 1.84
    {'symbol':'1INCHUSDT','param':(2400, 3100, 1)}, # AR 1.35, SHARPE 2.1
    {'symbol':'RNDRUSDT', 'param':(2800, 1300, 0)}, # AR 1.73, SHARPE 1.46
    {'symbol':'FETUSDT' , 'param':(4600, 1700, 0.25)}, # AR 1.06, SHARPE 1.87
    {'symbol':'ADAUSDT' , 'param':(400, 800, 1)}, # AR 1.02, SHARPE 1.98 > try 300, 700, 0.75
    {'symbol':'SANDUSDT', 'param':(2800, 500, 0)}, # AR 0.83, SHARPE 1.98 > try 2800, 500, 0
    {'symbol':'FILUSDT' , 'param':(1300, 100, 1)}, # AR 0.94, SHARPE 1.57
    {'symbol':'DOTUSDT' , 'param':(800, 3200, 0.25)}, # AR 1.06, SHARPE 1.4
    {'symbol':'APTUSDT' , 'param':(200, 1200, 0.5)}, # AR 1.45, SHARPE 2.01 / RARE TRADING CHANCE
    {'symbol':'IMXUSDT' , 'param':(4000, 1400, 0)}, # AR 1.04, SHARPE 1.45
    {'symbol':'MANAUSDT', 'param':(800, 1100, 0)}, # AR 0.98, SHARPE 1.45
    {'symbol':'WOOUSDT' , 'param':(100, 400, 1)}, # AR 0.98, SHARPE 1.45
    {'symbol':'ICPUSDT' , 'param':(800, 1900, 0)}, # AR 1.22, SHARPE 1.39
    {'symbol':'COMPUSDT', 'param':(100, 1300, 1.5)}, # AR 0.78, SHARPE 1.66
    {'symbol':'CHZUSDT' , 'param':(1500, 3300, 0.5)}, # AR 0.8, SHARPE 1.22
    {'symbol':'ETCUSDT',  'param':(900, 2100, 3)},  # AR 0.51, SHARPE 1.82 / RARE TRADING CHANCE

    # {'symbol':'FLOWUSDT', 'param':(700, 900, 1.5)}, # AR 1.05, SHARPE 1.51
    # {'BTCUSDT': (1300, 1600, 0)}, # AR 0.62, SHARPE 1.2
    # {'EOSUSDT': (200, 2100, 2)}, # AR 0.57, SHARPE 1.02

    # {'SEIUSDT': (2400, 2600, 0.25)}, # AR 3.83, SHARPE 3.88 / NEW LISTED
    # {'BLURUSDT': (2400, 800, 0.5)}, # AR 2.2, SHARPE 3.42 / NEW LISTED, NEED MORE DATA

]



def fetch_data(symbol, timeframe):
    if timeframe == '15min':
        klines_15min = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_15MINUTE, date_from, date_to)
        return klines_15min

    if timeframe == '1h':
        klines_1h = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1HOUR, date_from, date_to)
        return klines_1h

    if timeframe == '4h':
        klines_4h = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_4HOUR, date_from, date_to)
        return klines_4h

    if timeframe == '1d':
        klines_1d = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1DAY, date_from, date_to)
        return klines_1d

def form_data_frame(symbol, csv_name, timeframe):
    df = fetch_data(symbol, timeframe=timeframe) # 15min / 1h / 4h / 1d
    col = ['Date', 'open', 'high', 'low', 'close', 'volume', 'close time', 'Quote asset volume', 'Num trades', 'buy vol', 'buy asset volume', 'ignore.']
    df = pd.DataFrame(df, columns=col)
    df['Date'] = pd.to_datetime(df['Date'], unit='ms')
    df['close'] = pd.to_numeric(df['close'], errors='coerce')
    df.to_csv(csv_name, mode='w', index=False, header=True)

def write_csv(symbol, csv_name, timeframe):
    if not os.path.isfile(csv_name):
        print(f'create new csv for {symbol}')
        form_data_frame(symbol, csv_name, timeframe)
        time.sleep(3)
    else:
        print(f'use existing csv for {symbol}')
        time.sleep(3)

def create_merged_csv(csv_name):
    def get_oi_data(pages):
        data_list = []
        cursor = ''
        for _ in range(pages):
            response = bybit_request.get_open_interest(category=category, symbol=symbol, interval_time=timeframe, limit=limit, cursor=cursor)
            for i in response['list']:
                data_list.append(i)

            if 'nextPageCursor' in response:
                cursor = response['nextPageCursor']
            else:
                break

        print('Total rows:', len(data_list))

        col = ['timestamp', 'openInterest']
        df = pd.DataFrame(data_list, columns=col)
        df = df.rename(columns={'timestamp':'Date'})
        df['Date'] = pd.to_datetime(df['Date'], unit='ms').dt.round('H')
        # df['timestamp'] = pd.to_datetime(df['timestamp'], unit='ms')


        # print(df)
        return df

    if not os.path.isfile(csv_name):
        write_csv(symbol, csv_name, timeframe)
        df2 = pd.read_csv(csv_name)
        df2 = df2[['Date', 'close']]
        df2['Date'] = pd.to_datetime(df2['Date'])
        df2['Date'] = pd.to_datetime(df2['Date'], format='%d/%m/%Y %H:%M')
    else:
        df2 = pd.read_csv(csv_name)
        df2 = df2[['Date', 'close']]
        df2['Date'] = pd.to_datetime(df2['Date'])
        df2['Date'] = pd.to_datetime(df2['Date'], format='%d/%m/%Y %H:%M')

    num_rows = len(df2)
    pages = int(round((num_rows / 200), 0))

    df = get_oi_data(pages)


    merge_df = pd.merge(df, df2, on='Date')
    merge_df = merge_df.sort_values(ascending=True, by='Date').reset_index(drop=True)
    merge_df = merge_df.drop_duplicates(subset='Date', keep='first')
    merge_df.to_csv(f'merge_{symbol}_{timeframe}.csv', index=False)

    return merge_df



def bband(df, symbol, n, x, y):

    # df = pd.read_csv(f'merge_{symbol}_{timeframe}.csv')
    df['pct_change'] = df['close'].pct_change()
    df['ma_price'] = df['close'].rolling(n).mean()

    df['openInterest'] = pd.to_numeric(df['openInterest'])
    df['ma'] = df['openInterest'].rolling(x).mean()
    df['sd'] = df['openInterest'].rolling(x).std()
    df['z'] = (df['openInterest'] - df['ma']) / df['sd']
    df['+b'] = df['ma'] + (df['sd'] * y)
    df['-b'] = df['ma'] - (df['sd'] * y)

    # df['pos'] = np.where((df['z'] > y), np.where(df['close'] > df['ma_price'], -1, 1),
    #                      np.where(df['z'] < -y, 0, ?))

    # drop & +oi > up
    # up & +oi > drop

    # Open long if z > y and close > ma_price
    long_cond = (df['z'] > y) & ((df['close'] > df['ma_price']))

    # Open short if z > y and close < ma_price
    short_cond = (df['z'] > y) & ((df['close'] < df['ma_price']))

    # Hold if within +/- y band
    hold_cond = (np.abs(df['z']) <= y)

    df['pos'] = 0

    df['pos'] = np.where(long_cond, 1,
                         np.where(short_cond, -1,
                                  np.where(hold_cond, 0, df['pos'])))


    # df['pos'] = np.where((df['z'] > y) & (df['close'] > df['ma_price']), -1,
    #                      np.where((df['z'] > y) & (df['close'] < df['ma_price']), 1, 0))



    df['pos_t-1'] = df['pos'].shift(1)
    df['trade'] = abs(df['pos_t-1'] - df['pos'])
    df['cost'] = df['trade'] * 0.05 / 100
    df['pnl'] = df['pos_t-1'] * df['pct_change'] - df['cost']
    df['cumu'] = df['pnl'].cumsum()

    ### buy & hold cumu
    df['bnh_pnl'] = df['pct_change']
    df.loc[0:x - 1, 'bnh_pnl'] = 0
    df['bnh_cumu'] = df['bnh_pnl'].cumsum()
    df.loc[0:x - 1, 'bnh_cumu'] = 0
    df['bnh_cumu_ma'] = df['bnh_cumu'].rolling(n).mean()

    sharpe = df['pnl'].mean() / df['pnl'].std() * np.sqrt(365 * 24)
    sharpe = round(sharpe, 2)
    bnh_sharpe = df['bnh_pnl'].mean() / df['bnh_pnl'].std() * np.sqrt(365 * 24)
    bnh_sharpe = round(bnh_sharpe, 2)
    annual_return = round(df['pnl'].mean() * (365 * 24), 2)

    df['dd'] = df['cumu'].cummax() - df['cumu']
    mdd = round(df['dd'].max(), 3)
    calmar = round(annual_return / mdd, 2)
    # print(x, y, 'sharpe', sharpe, 'annual_return', annual_return, 'mdd', mdd, 'calmar', calmar)
    # print(f'x = {x}, y = {y}, sharpe = {sharpe}, annual_return = {annual_return}, mdd = {mdd}, calmar = {calmar}')

    stats = pd.Series([x, y, sharpe, annual_return, mdd, calmar],
                      index=['x', 'y', 'sharpe', 'annual_return', 'mdd', 'calmar'])


    fig = go.Figure()

    fig.add_trace(go.Scatter(x=df['Date'], y=df['cumu'], mode='lines', name='cumu'))
    fig.add_trace(go.Scatter(x=df['Date'], y=df['dd'], mode='lines', name='dd'))
    fig.add_trace(go.Scatter(x=df['Date'], y=df['bnh_cumu'], mode='lines', name='bnh_cumu'))
    fig.add_trace(go.Scatter(x=df['Date'], y=df['bnh_cumu_ma'], mode='lines', name='bnh_cumu_ma'))
    fig.add_trace(go.Scatter(x=df['Date'], y=df['openInterest'], mode='lines', name='openInterest', yaxis='y2'))
    fig.add_trace(go.Scatter(x=df['Date'], y=df['ma'], mode='lines', name='ma', yaxis='y2'))
    fig.add_trace(go.Scatter(x=df['Date'], y=df['+b'], mode='lines', name='+b', yaxis='y2'))
    fig.add_trace(go.Scatter(x=df['Date'], y=df['-b'], mode='lines', name='-b', yaxis='y2'))

    fig.update_layout(
        title=f'{symbol} oi Strategy price_ma = {n}, x={x}, y={y}, annual_return={annual_return}, sharpe={sharpe}',
        yaxis2=dict(title='openinterest',
                    overlaying='y',
                    side='right',
                    showgrid=False)
    )

    fig.show()




for pairs in param_list:
    symbol = pairs['symbol']
    param = pairs['param']
    csv_name = f'{symbol}_{timeframe}.csv'
    if not os.path.isfile(f'merge_{symbol}_{timeframe}.csv'):
        df = create_merged_csv(csv_name)
    else:
        print('use existing csv')
        df = pd.read_csv(f'merge_{symbol}_{timeframe}.csv')
        time.sleep(2)
    n, x, y = param
    bband(df, symbol, n, x, y)


