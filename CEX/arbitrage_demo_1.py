from CEX_Request_V3 import ByBitRequest, HashKeyRequest, BinanceRequest
import pal
import concurrent.futures
import requests
import aiohttp
import asyncio
import csv

bybit_key = pal.bybit_key
bybit_secret = pal.bybit_secret
hashkey_key = pal.hashkey_key
hashkey_secret = pal.hashkey_secret
binance_key = pal.binance_key
binance_secret = pal.binance_secret

binance_request = BinanceRequest(binance_key, binance_secret)
hashkey_request = HashKeyRequest(hashkey_key, hashkey_secret)

class DataFetcher:

    async def fetch_binance_data(self, session, symbol):
        url = 'https://api.binance.com/api/v3/ticker/bookTicker'
        headers = {'X-MBX-APIKEY': pal.binance_key}

        # Use aiohttp.ClientSession for making the request
        async with session.get(url, headers=headers) as response:
            if response.status == 200:
                data = await response.json()
                target_data = next((item for item in data if item['symbol'] == symbol), None)

                if target_data is None:
                    raise ValueError(f'Symbol {symbol} not found in the response data.')

                ask_price = float(target_data['askPrice'])
                bid_price = float(target_data['bidPrice'])

                return ask_price, bid_price
            else:
                raise Exception(f"Failed to fetch data from Binance: {response.status}")

    async def fetch_hashkey_data(self, session, symbol, limit=5):
        base_url = "https://api-pro.hashkey.com/quote/v1/depth"
        params = {"symbol": symbol, "limit": limit}
        async with session.get(base_url, params=params) as response:
            if response.status == 200:
                data = await response.json()
                ask_price = float(data['a'][0][0])  # Assuming 'a' is the key for asks
                bid_price = float(data['b'][0][0])  # Assuming 'b' is the key for bids
                return ask_price, bid_price
            else:
                print(f"Failed to fetch data: {response.status}")
                return None


async def execute_binance_order(symbol_binance, side, quantity):
    response = binance_request.place_order(
        symbol=symbol_binance,
        side=side,
        order_type='MARKET',
        quantity=quantity
    )
    print(f'Binance Order: symbol = {response}')



async def execute_hashkey_order(symbol_hashkey, side, quantity):
    response, orderId, clientOrderId = hashkey_request.create_order(
        symbol=symbol_hashkey,
        side=side,
        order_type='MARKET',
        quantity=quantity)
    print(f'Hashkey order: {response}')


async def check_for_arbitrage_and_execute_orders(binance_data, hashkey_data, symbol_binance, symbol_hashkey,
                                                 quantity, position_tracker):
    binance_ask, binance_bid = binance_data
    hashkey_ask, hashkey_bid = hashkey_data

    # Calculate both price differences
    binance_to_hashkey_price_diff = binance_bid - hashkey_ask
    hashkey_to_binance_price_diff = hashkey_bid - binance_ask

    # Define the thresholds
    open_threshold = -70
    close_threshold = -20

    # Check if there's an arbitrage opportunity to go long on Binance and short on Hashkey
    if binance_to_hashkey_price_diff <= open_threshold and position_tracker['current_position'] < position_tracker[
        'max_position']:
        # Open long position on Binance and short on Hashkey
        buy_binance = execute_binance_order(symbol_binance, side='BUY', quantity=quantity)
        sell_hashkey = execute_hashkey_order(symbol_hashkey, side='sell', quantity=quantity)

        # Wait for both orders to complete
        await asyncio.gather(buy_binance, sell_hashkey)
        position_tracker['current_position'] += 1  # Increment because we're taking a long position on Binance

    elif binance_to_hashkey_price_diff > close_threshold and position_tracker['current_position'] > 0:
        # Close long position on Binance and short on Hashkey
        sell_binance = execute_binance_order(symbol_binance, side='SELL', quantity=quantity)
        buy_hashkey = execute_hashkey_order(symbol_hashkey, side='buy', quantity=round(quantity * hashkey_ask, 5))

        # Wait for both orders to complete
        await asyncio.gather(sell_binance, buy_hashkey)
        position_tracker['current_position'] -= 1  # Decrement because we're closing a long position on Binance

    # Check if there's an arbitrage opportunity to go long on Hashkey and short on Binance
    elif hashkey_to_binance_price_diff <= open_threshold and position_tracker['current_position'] > -position_tracker[
        'max_position']:
        # Open long position on Hashkey and short on Binance
        buy_hashkey = execute_hashkey_order(symbol_hashkey, side='buy', quantity=round(quantity * hashkey_ask, 5))
        sell_binance = execute_binance_order(symbol_binance, side='SELL', quantity=quantity)

        # Wait for both orders to complete
        await asyncio.gather(buy_hashkey, sell_binance)
        position_tracker['current_position'] -= 1  # Decrement because we're taking a short position on Binance

    elif hashkey_to_binance_price_diff > close_threshold and position_tracker['current_position'] < 0:
        # Close long position on Hashkey and short on Binance
        sell_hashkey = execute_hashkey_order(symbol_hashkey, side='sell', quantity=quantity)
        buy_binance = execute_binance_order(symbol_binance, side='BUY', quantity=quantity)

        # Wait for both orders to complete
        await asyncio.gather(sell_hashkey, buy_binance)
        position_tracker['current_position'] += 1  # Increment because we're closing a short position on Binance

    # Return both price differences for logging or further analysis
    return binance_to_hashkey_price_diff, hashkey_to_binance_price_diff

current_position = 0  # Positive numbers for buys, negative for sells
# max_position = 2  # Maximum position size


async def main():
    fetcher = DataFetcher()
    symbol_binance = 'BTCFDUSD'
    symbol_hashkey = 'BTCUSD'
    quantity = 0.0005  # BTC
    position_tracker = {
        'current_position': 0,
        'max_position': 4,
    }
    price_difference = 0.0  # Initialize price difference

    async with aiohttp.ClientSession() as session:
        while True:
            binance_task = asyncio.create_task(fetcher.fetch_binance_data(session, symbol_binance))
            hashkey_task = asyncio.create_task(fetcher.fetch_hashkey_data(session, symbol_hashkey))

            binance_data, hashkey_data = await asyncio.gather(binance_task, hashkey_task)
            if binance_data and hashkey_data:
                # Update price_difference only when a new one is found
                new_price_difference = await check_for_arbitrage_and_execute_orders(
                    binance_data, hashkey_data, symbol_binance, symbol_hashkey, quantity, position_tracker
                )
                if new_price_difference != 0.0:  # Check if an update occurred
                    price_difference = new_price_difference

                print(
                    f"Current Position: {position_tracker['current_position']}, Last Price Difference: {price_difference}\n"
                    f"=====================================================")
            else:
                print("Failed to fetch data from one or both exchanges.")

            await asyncio.sleep(1)  # Sleep for some time before the next iteration

asyncio.run(main())