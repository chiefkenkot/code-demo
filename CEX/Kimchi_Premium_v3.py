import datetime
import time
import requests
from pprint import pprint
from CEX_Request_V3 import BinanceRequest, Support_Function
import pal

binance_key = pal.binance_key
binance_secret = pal.binance_secret

binance_request = BinanceRequest(binance_key, binance_secret)
support_function = Support_Function()


# url = 'https://api.upbit.com/v1/ticker?markets=USDT-SC' # get ticker info, format USDT-{coin}
# url = 'https://api.upbit.com/v1/candles/minutes/5?market=USDT-SC&count=3' # get ticker info, format USDT-{coin}


def fetch_upbit_pair():
    url = 'https://api.upbit.com/v1/market/all'
    response = requests.get(url)
    response.raise_for_status()
    response_data = response.json()

    upbit_dict = {}

    for info in response_data:
        symbol = info['market']
        if symbol.startswith('KRW-'):
            base_currency = symbol.split('-')[1]  # Split the string and get the second element
            upbit_dict[base_currency] = symbol

    return upbit_dict

def fetch_binance_pair():
    url = 'https://api.binance.com/api/v3/exchangeInfo'
    response = requests.get(url)
    response.raise_for_status()
    response_data = response.json()
    # pprint(response_data)

    binance_dict = {}

    for info in response_data['symbols']:
        symbol = info['symbol']
        status = info['status']
        if symbol.endswith('USDT'):
            if status == 'TRADING':
                base_currency = symbol.replace('USDT', '')
                binance_dict[base_currency] = symbol

    return binance_dict

# dict = fetch_binance_pair()
# print(dict)
# print(len(dict))
# time.sleep(9999)

def krw_conversion():
    # Replace 'your_api_key' with the API key you obtained from CoinMarketCap
    headers = {
        'X-CMC_PRO_API_KEY': pal.coin_market_cap,
    }

    # This endpoint retrieves the latest market quote for 1 or more cryptocurrencies
    api_url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest'

    # We are using the CoinMarketCap ID for USDT and converting it to KRW
    parameters = {
        'id': '825',  # The CoinMarketCap ID for Tether (USDT)
        'convert': 'KRW'  # Convert it to South Korean Won
    }

    response = requests.get(api_url, headers=headers, params=parameters)

    if response.status_code == 200:
        data = response.json()
        # Navigate through the response to get the price
        usdt_to_krw_rate = data['data']['825']['quote']['KRW']['price']
        # print(f"The current exchange rate for USDT to KRW is: {usdt_to_krw_rate}")
    else:
        print(f"Failed to fetch the exchange rate. Status code: {response.status_code}")

    return usdt_to_krw_rate

# rate = krw_conversion()
# print(rate)
# time.sleep(9999)

def fetch_upbit_market_data(symbol):

    url = f'https://api.upbit.com/v1/ticker?markets=KRW-{symbol}'
    response = requests.get(url)
    if response.status_code == 200:
        response = response.json()
        # pprint(response)
        return response
    else:
        print(f"Failed to fetch data for KRW-{symbol}")

# krw = krw_conversion()
# print(krw)
# # time.sleep(9999)

def check_kimchi_premium(kimchi_old_list, krw_rate):
    binance_token = fetch_binance_pair()
    upbit_toekn = fetch_upbit_pair()
    common_token = set(binance_token.keys()) & set(upbit_toekn.keys())
    common_token = sorted(common_token)

    print(binance_token)
    print(len(binance_token))
    print(upbit_toekn)
    print(len(upbit_toekn))
    print(sorted(common_token))
    print(len(common_token))
    # time.sleep(9999)

    #
    # krw_rate = 1319.5209298543643
    print(f"The current exchange rate for USDT to KRW is: {krw_rate}")

    kimchi_new_list = {}

    for token in common_token:
        binance_ask, binance_bid = binance_request.fetch_binance_data(binance_symbol=f'{token}USDT')
        # print(f'Binance: {token}USDT: {binance_ask}')
        response = fetch_upbit_market_data(symbol=token)
        # trade_volume = round(float(response[0]['acc_trade_volume_24h']), 2)
        trade_price = round(float(response[0]['trade_price']), 2)
        # monetary_volume = trade_price * trade_volume
        usd_trade_price = trade_price / krw_rate
        diff = (usd_trade_price / binance_ask) - 1
        message = f'{token}\nUpbit: {usd_trade_price}\nBinance: {binance_ask}\nSpread: {diff}'
        print(message)

        if diff >= 0.2:
            # add order functon later on
            message = f'{token}\nUpbit: {usd_trade_price}\nBinance: {binance_ask}\nSpread: {diff}'
            # print(message)
            # kimchi_new_list.append(f'{token}USDT')
            kimchi_new_list[f'{token}USDT'] = diff

        print('===================================================')

    # Now compare the new list with the old list
    new_entries = set(kimchi_new_list.keys()) - set(kimchi_old_list.keys())
    removed_entries = set(kimchi_old_list.keys()) - set(kimchi_new_list.keys())

    # Send a Telegram pop-up only if there are newly added symbols with their spread values
    if new_entries:
        new_entries_dict = {symbol: kimchi_new_list[symbol] for symbol in new_entries}
        new_entries_str = ", ".join(
            f"{symbol}: {spread:.2%}" for symbol, spread in new_entries_dict.items())  # Formatting spread as percentage
        print(f"Newly added symbols with Kimchi premium:\n{new_entries_dict}")
        support_function.tg_pop(f'New symbol with Kimchi premium:\n{new_entries_str}')
        # Here you can handle the new entries (e.g., place orders)

    # Send a Telegram pop-up only if there are symbols removed with their last known spread values
    if removed_entries:
        removed_entries_dict = {symbol: kimchi_old_list[symbol] for symbol in removed_entries}
        removed_entries_str = ", ".join(f"{symbol}: {spread:.2%}" for symbol, spread in
                                        removed_entries_dict.items())  # Formatting spread as percentage
        print(f"Symbols removed from Kimchi premium:\n{removed_entries_dict}")
        support_function.tg_pop(f'Removed symbol with Kimchi premium:\n{removed_entries_str}')
        # Here you can handle the removed entries (e.g., cancel orders)


    print(kimchi_new_list)

    # Return the new list so it can be used as the old list on the next run
    return kimchi_new_list


kimchi_old_list = {}
krw_rate = 0

while True:
    print(f'Time: {datetime.datetime.now()}')
    if datetime.datetime.now().minute <= 20:
        krw_rate = krw_conversion()
    if datetime.datetime.now().hour == 0:
        support_function.tg_pop('Kimchi Script still alive')

    # Inside your while loop, you call the function and update kimchi_old_list
    kimchi_old_list = check_kimchi_premium(kimchi_old_list, krw_rate)

    print('======================== Loop End ========================')
    time.sleep(900)