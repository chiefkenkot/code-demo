import pandas as pd
import numpy as np
import requests
import time
from binance.client import Client
import pal
import os
from Strategy import strategy
import datetime
from CEX_Request_V3 import ByBitRequest, Support_Function
from pprint import pprint

strategy = strategy()

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', 1000)

client = Client(pal.binance_key, pal.binance_secret)
bybit_key = pal.bybit_key
bybit_secret = pal.bybit_secret
bybit_request = ByBitRequest(bybit_key, bybit_secret)
support_function = Support_Function()


current_date = datetime.datetime.now()
formatted_date = current_date.strftime("%d %b, %Y")
hr = datetime.datetime.now().hour


symbol_list = ['BTCUSDT', 'APTUSDT', 'SOLUSDT', 'INJUSDT', 'NEARUSDT', 'AVAXUSDT', 'OPUSDT']
hybrid_pair = [('BTCUSDT', 'SOLUSDT', 2000, 2100, 0.75, 3.5),
                    ('BTCUSDT', 'INJUSDT', 900, 2000, 0.25, 3.5),
                    ('BTCUSDT', 'NEARUSDT', 1400, 2100, 0.5, 3.5),
                    ('APTUSDT', 'AVAXUSDT', 1100, 2600, 1, 2.5),
                    ('APTUSDT', 'SOLUSDT', 300, 2600, 0.25, 2.25)]

momentum_pair = [('BTCUSDT', 'APTUSDT', 1300, 2.25),
                 ('OPUSDT', 'NEARUSDT', 300, 0.25)]


# Full Cycle
date_from = "12 May, 2020"
date_to = formatted_date

timeframe = '1h' # 15min / 1h / 4h / 1d


def fetch_data(symbol, timeframe):
    if timeframe == '15min':
        klines_15min = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_15MINUTE, date_from, date_to)
        return klines_15min

    if timeframe == '1h':
        klines_1h = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1HOUR, date_from, date_to)
        return klines_1h

    if timeframe == '4h':
        klines_4h = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_4HOUR, date_from, date_to)
        return klines_4h

    if timeframe == '1d':
        klines_1d = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1DAY, date_from, date_to)
        return klines_1d


def form_data_frame(symbol, csv_name, timeframe):
    df = fetch_data(symbol, timeframe=timeframe)  # 15min / 1h / 4h / 1d
    col = ['Date', 'open', 'high', 'low', 'close', 'volume', 'close time', 'Quote asset volume', 'Num trades',
           'buy vol', 'buy asset volume', 'ignore.']
    df = pd.DataFrame(df, columns=col)
    df['Date'] = pd.to_datetime(df['Date'], unit='ms')
    df['close'] = pd.to_numeric(df['close'], errors='coerce')

    # Fetch new data
    data = bybit_request.get_kline('linear', symbol, '60', limit=hr)
    data = data['list']
    col = ['Date', 'open', 'high', 'low', 'close', 'volume', 'turnover']
    df2 = pd.DataFrame(data=data, columns=col)
    df2['Date'] = pd.to_datetime(df2['Date'], unit='ms')
    df2['Date'] = df2['Date'].dt.strftime('%d/%m/%Y %H:%M')

    # Reverse the DataFrame to have the latest data at the bottom
    df2 = df2.iloc[::-1].reset_index(drop=True)

    # Align df2 to have the same columns as df, with NaNs where data is not available
    df2 = df2.reindex(columns=df.columns)

    # Now that both DataFrames are aligned, concatenate them
    df = pd.concat([df, df2], ignore_index=True)

    # Save the final DataFrame to CSV
    df.to_csv(csv_name, mode='w', index=False, header=True)


def write_csv(symbol, csv_name, timeframe):
    if not os.path.isfile(csv_name):
        print('create new csv')
        form_data_frame(symbol, csv_name, timeframe)
        time.sleep(3)
    else:
        print('use existing csv')
        time.sleep(3)


def fetch_all_data():
    for symbol in symbol_list:
        csv_name = f'{symbol}_{timeframe}.csv'
        df = pd.read_csv(csv_name)

        data = bybit_request.get_kline('linear', symbol, '1', limit=1)
        data = data['list']
        col = ['Date', 'open', 'high', 'low', 'close', 'volume', 'turnover']
        df2 = pd.DataFrame(data=data, columns=col)
        df2['Date'] = pd.to_datetime(df2['Date'], unit='ms')
        df2['Date'] = df2['Date'].dt.strftime('%d/%m/%Y %H:%M')

        # Reverse the DataFrame to have the latest data at the bottom
        df2 = df2.iloc[::-1].reset_index(drop=True)

        # Align df2 to have the same columns as df, with NaNs where data is not available
        df2 = df2.reindex(columns=df.columns)

        # Now that both DataFrames are aligned, concatenate them
        df = pd.concat([df, df2], ignore_index=True)

        # Save the final DataFrame to CSV
        df.to_csv(csv_name, mode='w', index=False, header=True)

        print(f'CSV {csv_name} updated')

def cross_asset_bband_hybrid(pairs, timeframe):
    lead_symbol, lag_symbol, x1, x2, y1, y2 = pairs

    df1 = pd.read_csv(f'{lead_symbol}_{timeframe}.csv')
    df1 = df1[['Date', 'close']]

    df2 = pd.read_csv(f'{lag_symbol}_{timeframe}.csv')
    df2 = df2[['Date', 'close']]
    df2 = df2.rename(columns={'close': 'value'})

    merge_df = pd.merge(df1, df2, on='Date', how='inner')

    merge_df['pct_change'] = merge_df['value'].pct_change()

    df = merge_df

    df['ma1'] = df['close'].rolling(x1).mean()
    df['sd1'] = df['close'].rolling(x1).std()
    df['z1'] = (df['close'] - df['ma1']) / df['sd1']

    df['pos1'] = np.where(df['z1'] > y1, 1, np.where(df['z1'] < -y1, -1, 0))

    df['ma2'] = df['close'].rolling(x2).mean()
    df['sd2'] = df['close'].rolling(x2).std()
    df['z2'] = (df['close'] - df['ma2']) / df['sd2']

    df['pos2'] = np.where(df['z2'] > y2, -2, np.where(df['z2'] < -y2, 2, 0))

    df['pos'] = df['pos1'] + df['pos2']

    hybrid_current_pos = df['pos'].iloc[-1]
    hybrid_previous_pos = df['pos'].iloc[-2]

    return hybrid_current_pos, hybrid_previous_pos, lag_symbol


def cross_asset_bband(pairs, timeframe):
    lead_symbol, lag_symbol, x, y = pairs

    df1 = pd.read_csv(f'{lead_symbol}_{timeframe}.csv')
    df1 = df1[['Date', 'close']]

    df2 = pd.read_csv(f'{lag_symbol}_{timeframe}.csv')
    df2 = df2[['Date', 'close']]
    df2 = df2.rename(columns={'close': 'value'})

    merge_df = pd.merge(df1, df2, on='Date', how='inner')

    merge_df['pct_change'] = merge_df['value'].pct_change()

    df = merge_df

    df['ma'] = df['close'].rolling(x).mean()
    df['sd'] = df['close'].rolling(x).std()
    df['z'] = (df['close'] - df['ma']) / df['sd']

    df['pos'] = np.where(df['z'] > y, 1, np.where(df['z'] < -y, -1, 0))

    current_pos = df['pos'].iloc[-1]
    previous_pos = df['pos'].iloc[-2]

    return current_pos, previous_pos, lag_symbol


# initial documents
for symbol in symbol_list:
    csv_name = f'{symbol}_{timeframe}.csv'
    write_csv(symbol=symbol, csv_name=csv_name, timeframe=timeframe)


size_map = {
    'SOLUSDT': 0.5,
    'INJUSDT': 1.5,
    'NEARUSDT': 20,
    'AVAXUSDT': 2,
    'APTUSDT': 8
}

trade_amount = 50


while True:
    current_time = datetime.datetime.now()
    if current_time.minute == 0 and current_time.second <= 5:
        time.sleep(120)
        try:
            fetch_all_data()
            for pairs in hybrid_pair:
                hybrid_current_pos, hybrid_previous_pos, lag_symbol = cross_asset_bband_hybrid(pairs, timeframe)
                print(f'{lag_symbol} previous pos = {hybrid_previous_pos}, current pos = {hybrid_current_pos}')
                if hybrid_previous_pos != hybrid_current_pos:
                    pos = hybrid_current_pos - hybrid_previous_pos
                    bid, ask = bybit_request.fetch_bybit_data(symbol=lag_symbol, category='linear')
                    size = size_map[lag_symbol] * pos
                    print(f'{lag_symbol} pos = {pos}')
                    if pos > 0:
                        print(f'sending {lag_symbol} {pos} buy order')
                        response = bybit_request.place_market_order(category='linear', side='Buy', symbol=lag_symbol,
                                                                    order_type='Market', qty=size)
                        message = f'Bybit Long {lag_symbol}\nPos = {hybrid_current_pos}\nRef Price = {ask}\nsize = {size}({pos})'
                        print(message)
                        support_function.tg_pop(message)
                    if pos < 0:
                        print(f'sending {lag_symbol} {pos} sell order')
                        response = bybit_request.place_market_order(category='linear', side='Sell', symbol=lag_symbol,
                                                                    order_type='Market', qty=size)
                        message = f'Bybit Short {lag_symbol}\nPos = {hybrid_current_pos}\nRef Price = {ask}\nsize = {size}({pos})'
                        print(message)
                        support_function.tg_pop(message)
                print('========================== Next Symbol ==========================')

            for pairs in momentum_pair:
                current_pos, previous_pos, lag_symbol = cross_asset_bband(pairs, timeframe)
                print(f'{lag_symbol} previous pos = {previous_pos}, current pos = {current_pos}')
                if previous_pos != current_pos:
                    pos = current_pos - previous_pos
                    bid, ask = bybit_request.fetch_bybit_data(symbol=lag_symbol, category='linear')
                    size = size_map[lag_symbol] * pos
                    print(f'{pairs} pos = {pos}')
                    if pos > 0:
                        print(f'sending {lag_symbol} {pos} buy order')
                        response = bybit_request.place_market_order(category='linear', side='Buy', symbol=lag_symbol,
                                                                    order_type='Market', qty=size)
                        message = f'Bybit Long {lag_symbol}\nPos = {current_pos}\nRef Price = {ask}\nsize = {size}({pos})'
                        print(message)
                        support_function.tg_pop(message)
                    if pos < 0:
                        print(f'sending {lag_symbol} {pos} sell order')
                        response = bybit_request.place_market_order(category='linear', side='Sell', symbol=lag_symbol,
                                                                    order_type='Market', qty=size)
                        message = f'Bybit Short {lag_symbol}\nPos = {current_pos}\nRef Price = {ask}\nsize = {size}({pos})'
                        print(message)
                        support_function.tg_pop(message)
                print('========================== Next Symbol ==========================')
            print('========================== Loop End ==========================')

        except requests.exceptions.RequestException as e:
            support_function.tg_pop('Pair Trade Data Gathering ConnectionResetError Occurs, reboot now')
            continue

        except Exception as e:
            support_function.tg_pop(e)
            continue
        # Calculate how many seconds to sleep until the start of the next hour
        support_function.tg_pop('Pair Trade testing')
        # Calculate how many seconds to sleep until the start of the next hour
        current_time = datetime.datetime.now()  # Update current_time after processing is complete
        time_to_sleep = (3600 - current_time.minute * 60 - current_time.second)
        time.sleep(time_to_sleep)
    else:
        # Calculate how many seconds to sleep until the start of the next hour
        time_to_sleep = (3600 - current_time.minute * 60 - current_time.second)
        time.sleep(time_to_sleep)
